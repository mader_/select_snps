#!/usr/bin/env ruby

=begin
  Copyright (c) 2020 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2020 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require_relative 'lib/vcf'
require_relative 'lib/vcf_entry'
require_relative 'lib/plotter'
require_relative 'lib/compare_genotypes_option_parser.rb'

options = OptionParser.parse(ARGV)

vcf_file_1 = options.vcf_file_1
vcf_file_2 = options.vcf_file_2
type = options.type
out = options.output

#vcf_file1 = "Freebayes_nc_cp_mt_mindp3_red.vcf"
#vcf_file2 = "SNP_Data_MassArray_new_ids_red.vcf"
#out = "compare.out"

begin

  vcf1 = VCF.new
  vcf1.read_file(vcf_file_1, "compare", 0, 1)

  vcf2 = VCF.new
  vcf2.read_file(vcf_file_2, "compare", 0, 1)
  
  cmp = vcf1.cmp_gt(vcf2, type)

  plotter = Plotter.new

  plotter.write_cmp(cmp, out)

rescue RuntimeError => e
  puts e.message
end