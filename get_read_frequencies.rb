#!/usr/bin/env ruby

=begin
  Copyright (c) 2020-2021 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2020-2021 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require_relative 'lib/vcf'
require_relative 'lib/vcf_entry'
require_relative 'lib/get_read_frequencies_option_parser.rb'
require_relative 'lib/plotter'

options = OptionParser.parse(ARGV)

vcf_file = options.vcf_file
file_format = options.output_format
out = options.output
flank_length = 0

vcf = VCF.new
vcf.read_file(vcf_file, "single", flank_length, 1)

plotter = Plotter.new

plotter.write_vcf_read_frequencies(vcf, file_format, out)
