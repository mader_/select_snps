#!/usr/bin/env ruby

=begin
  Copyright (c) 2018 - 2019 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2018 - 2019 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require 'spec_helper'
require 'vcf'

require 'byebug'

describe VCF do
  it "reads one vcf file" do
  	file = Dir.pwd + "/data/testdata/genotypes_50x50_contig.vcf"
  	flank_length = 50
  	multiplier = 0.5
    vcf = VCF.new
  	vcf.read_file(file, "single", flank_length, multiplier)
  	expect(vcf.vcf_header.to_s_vcf).to eq("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tENUTI_74_1_2\tENUTI_107_1_2\tENUTI_97_3a_2")
  	expect(vcf.flanking_sequence_length).to eq(50)
  	expect(vcf.multiplier).to eq(0.5)
  	expect(vcf.vcf_entries.size).to eq(6)
  	expect(vcf.vcf_entries["RAD_contig_1_39_C"].ref).to eq("T")
  	expect(vcf.vcf_entries["RAD_contig_1_39_C"].qual).to eq(106)
  	expect(vcf.vcf_entries["RAD_contig_1_39_C"].info["AS"]).to eq("C[T/C]A")
  	expect(vcf.vcf_entries["RAD_contig_1_39_C"].format).to eq("GT:DP:GQ:AD")
  	expect(vcf.vcf_entries["RAD_contig_1_39_C"].genotypes_to_s).to eq("\t0/0:52:78:52,0\t1/1:31:47:0,31\t0/0:39:59:39,0")
  end

  it "merges two vcf files" do

  	file1 = Dir.pwd + "/data/testdata/genotypes_50x50_contig.vcf"
  	flank_length1 = 50
  	multiplier1 = 0.5

  	file2 = Dir.pwd + "/data/testdata/genotypes_75x75_contig.vcf"
  	flank_length2 = 75
  	multiplier2 = 0.75

    vcf1 = VCF.new
  	vcf1.read_file(file1, "single", flank_length1, multiplier1)
    vcf2 = VCF.new
  	vcf2.read_file(file2, "single", flank_length2, multiplier2)

    vcfs = [vcf1, vcf2]

  	mvcf = vcf1.merge_vcfs(vcfs)

  	expect(mvcf.vcf_entries.size).to eq(6)
  	expect(mvcf.vcf_entries["RAD_contig_1_39_C"].info["AS"]).to eq("GC[T/C]AG")
  	expect(mvcf.vcf_entries["RAD_contig_1_39_C"].ref).to eq("T")
  	expect(mvcf.vcf_entries["RAD_contig_1_39_C"].qual).to eq(106)
  	expect(mvcf.vcf_entries["RAD_contig_1_39_C"].genotypes_to_s).to eq("\t0/0:52:78:52,0\t1/1:31:47:0,31\t0/0:39:59:39,0")

  	expect(mvcf.vcf_entries["RAD_contig_3_175_T"].info["AS"]).to eq("C[G/T]A")
  	expect(mvcf.vcf_entries["RAD_contig_3_175_T"].ref).to eq("G")
  	expect(mvcf.vcf_entries["RAD_contig_3_175_T"].qual).to eq(11188)
  	expect(mvcf.vcf_entries["RAD_contig_3_175_T"].genotypes_to_s).to eq("\t0/0:145:218:145,0\t1/1:89:134:0,89\t0/0:233:330:232,1")

  end

  it "merges three vcf files" do
  	file1 = Dir.pwd + "/data/testdata/genotypes_50x50_contig.vcf"
  	flank_length1 = 50
  	multiplier1 = 0.5

  	file2 = Dir.pwd + "/data/testdata/genotypes_75x75_contig.vcf"
  	flank_length2 = 75
  	multiplier2 = 0.75

  	file3 = Dir.pwd + "/data/testdata/genotypes_100x100_contig.vcf"
  	flank_length3 = 100
  	multiplier3 = 1

    vcf1 = VCF.new
  	vcf1.read_file(file1, "single", flank_length1, multiplier1)
    vcf2 = VCF.new
  	vcf2.read_file(file2, "single", flank_length2, multiplier2)
    vcf3 = VCF.new
  	vcf3.read_file(file3, "single", flank_length3, multiplier3)

    vcfs = [vcf1, vcf2, vcf3]

  	mvcf = vcf1.merge_vcfs(vcfs)

	expect(mvcf.vcf_entries.size).to eq(6)
  	expect(mvcf.vcf_entries["RAD_contig_1_39_C"].info["AS"]).to eq("TGC[T/C]AGG")
  	expect(mvcf.vcf_entries["RAD_contig_1_39_C"].ref).to eq("T")
  	expect(mvcf.vcf_entries["RAD_contig_1_39_C"].qual).to eq(106)
  	expect(mvcf.vcf_entries["RAD_contig_1_39_C"].wqual).to eq(106)
  	expect(mvcf.vcf_entries["RAD_contig_1_39_C"].genotypes_to_s).to eq("\t0/0:52:78:52,0\t1/1:31:47:0,31\t0/0:39:59:39,0")

    expect(mvcf.vcf_entries["RAD_contig_2_157_A"].info["AS"]).to eq("GG[C/A]CC")
    expect(mvcf.vcf_entries["RAD_contig_2_157_A"].ref).to eq("C")
  	expect(mvcf.vcf_entries["RAD_contig_2_157_A"].qual).to eq(206)
  	expect(mvcf.vcf_entries["RAD_contig_2_157_A"].wqual).to eq(154.5)
  	expect(mvcf.vcf_entries["RAD_contig_2_157_A"].genotypes_to_s).to eq("\t0/0:77:116:77,0\t0/1:53:80:53,46\t0/0:48:206:34,0")

  	expect(mvcf.vcf_entries["RAD_contig_3_175_T"].info["AS"]).to eq("C[G/T]A")
  	expect(mvcf.vcf_entries["RAD_contig_3_175_T"].ref).to eq("G")
  	expect(mvcf.vcf_entries["RAD_contig_3_175_T"].qual).to eq(11188)
  	expect(mvcf.vcf_entries["RAD_contig_3_175_T"].wqual).to eq(5594)
  	expect(mvcf.vcf_entries["RAD_contig_3_175_T"].genotypes_to_s).to eq("\t0/0:145:218:145,0\t1/1:89:134:0,89\t0/0:233:330:232,1")

  end

  it "filters vcf file for missing flanking sequence" do
  	file2 = Dir.pwd + "/data/testdata/genotypes_75x75_contig.vcf"
  	flank_length2 = 75
  	multiplier2 = 0.75

    vcf2 = VCF.new
  	vcf2.read_file(file2, "single", flank_length2, multiplier2)

    filter = Hash.new()
    filter.store("discard_flank", 1)

    vcf2.filter_vcf(filter)
  	fvcf = vcf2

  	expect(fvcf.vcf_entries.size).to eq(4)
  	expect(fvcf.vcf_entries.key?("RAD_contig_1_39_C")).to be(true)
  	expect(fvcf.vcf_entries.key?("RAD_contig_1_104_G")).to be(true)
  	expect(fvcf.vcf_entries.key?("RAD_contig_2_157_A")).to be(true)
  	expect(fvcf.vcf_entries.key?("RAD_contig_2_300_C")).to be(true)
  	expect(fvcf.vcf_entries.key?("RAD_contig_3_175_T")).to be(false)
  	expect(fvcf.vcf_entries.key?("RAD_contig_3_304_G")).to be(false)
  end

  it "filters vcf file for quality" do
  	file2 = Dir.pwd + "/data/testdata/genotypes_75x75_contig.vcf"
  	flank_length2 = 75
  	multiplier2 = 0.75

    vcf2 = VCF.new
  	vcf2.read_file(file2, "single", flank_length2, multiplier2)

    filter = Hash.new()
    filter.store("qual", 300)

    vcf2.filter_vcf(filter)
  	fvcf = vcf2

  	expect(fvcf.vcf_entries.size).to eq(4)
  	expect(fvcf.vcf_entries.key?("RAD_contig_1_39_C")).to be(false)
  	expect(fvcf.vcf_entries.key?("RAD_contig_1_104_G")).to be(true)
  	expect(fvcf.vcf_entries.key?("RAD_contig_2_157_A")).to be(false)
  	expect(fvcf.vcf_entries.key?("RAD_contig_2_300_C")).to be(true)
  	expect(fvcf.vcf_entries.key?("RAD_contig_3_175_T")).to be(true)
  	expect(fvcf.vcf_entries.key?("RAD_contig_3_304_G")).to be(true)
  end

  it "filters vcf file for flank and quality" do
  	file2 = Dir.pwd + "/data/testdata/genotypes_75x75_contig.vcf"
  	flank_length2 = 75
  	multiplier2 = 0.75

    vcf2 = VCF.new
  	vcf2.read_file(file2, "single", flank_length2, multiplier2)

    filter = Hash.new()
    filter.store("discard_flank", 1)
    filter.store("qual", 300)

    vcf2.filter_vcf(filter)
  	fvcf = vcf2

  	expect(fvcf.vcf_entries.size).to eq(2)
  	expect(fvcf.vcf_entries.key?("RAD_contig_1_39_C")).to be(false)
  	expect(fvcf.vcf_entries.key?("RAD_contig_1_104_G")).to be(true)
  	expect(fvcf.vcf_entries.key?("RAD_contig_2_157_A")).to be(false)
  	expect(fvcf.vcf_entries.key?("RAD_contig_2_300_C")).to be(true)
  	expect(fvcf.vcf_entries.key?("RAD_contig_3_175_T")).to be(false)
  	expect(fvcf.vcf_entries.key?("RAD_contig_3_304_G")).to be(false)
  end

  it "processes one vcf file" do
    file = Dir.pwd + "/data/testdata/genotypes_100x100.vcf"
    flank_length = 100
    multiplier = 1
    prefix = "test_snps"

    vcf1 = VCF.new
    vcf1.read_file(file, "single", flank_length, multiplier)

    vcfs = [vcf1]

    mvcf = vcf1.merge_vcfs(vcfs)

    filter = Hash.new()
    mvcf.filter_vcf(filter)
    fvcf = mvcf

    fvcf.sort_and_group_vcf(prefix)
    svcf = fvcf

    expect(svcf.multiplier).to eq(1)
    expect(svcf.flanking_sequence_length).to eq(0)
    expect(svcf.vcf_header.genotypes_to_s).to eq("\tENUTI_74_1_2\tENUTI_107_1_2\tENUTI_97_3a_2")
    expect(svcf.vcf_entries.size).to eq(18)
    expect(svcf.grouped_vcf_entries.size).to eq(7)
    expect(svcf.groups_sorted_by_qual.size).to eq(7)
    expect(svcf.groups_sorted_by_wqual.size).to eq(7)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"].size).to eq(6)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"].size).to eq(4)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"].size).to eq(3)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"].size).to eq(2)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"].size).to eq(1)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"].size).to eq(1)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"].size).to eq(1)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].chrom).to eq("RAD_contig_14")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].pos).to eq(280)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_14_280")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].qual).to eq(500)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].wqual).to eq(500)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].info["AS"]).to eq(".")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].genotypes_to_s).to eq("\t0/0:40:60:40,0\t1/1:483:727:0,483\t0/0:308:1768:199,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].chrom).to eq("RAD_contig_18")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].pos).to eq(587)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].id).to eq("test_snps_RAD_contig_18_587")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].qual).to eq(465)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].wqual).to eq(465)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].info["AS"]).to eq(".")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].genotypes_to_s).to eq("\t0/0:28:42:28,0\t1/1:309:465:0,309\t0/0:305:438:304,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].chrom).to eq("RAD_contig_5")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].pos).to eq(175)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].id).to eq("test_snps_RAD_contig_5_175")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].alt).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].qual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].wqual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].info["AS"]).to eq("CCG[G/T]AGC")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].genotypes_to_s).to eq("\t0/0:145:218:145,0\t1/1:89:134:0,89\t0/0:233:330:232,1")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].chrom).to eq("RAD_contig_9")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].pos).to eq(248)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].id).to eq("test_snps_RAD_contig_9_248")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].qual).to eq(150)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].wqual).to eq(150)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].info["AS"]).to eq("GAG[T/C]CGC")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].genotypes_to_s).to eq("\t0/0:188:283:188,0\t1/1:19:29:0,190\t0/0:280:421:280,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].chrom).to eq("RAD_contig_1")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].pos).to eq(39)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].id).to eq("test_snps_RAD_contig_1_39")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].qual).to eq(106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].wqual).to eq(106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].info["AS"]).to eq("TGC[T/C]AGG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].genotypes_to_s).to eq("\t0/0:52:78:52,0\t1/1:31:47:0,31\t0/0:39:59:39,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].chrom).to eq("RAD_contig_16")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].pos).to eq(604)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].id).to eq("test_snps_RAD_contig_16_604")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].qual).to eq(87)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].wqual).to eq(87)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].info["AS"]).to eq(".")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].genotypes_to_s).to eq("\t0/0:20:30:20,0\t1/1:58:87:0,58\t0/0:67:101:67,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].chrom).to eq("RAD_contig_6")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].pos).to eq(304)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_6_304")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].qual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].wqual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].info["AS"]).to eq("GGG[A/G]AAG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].genotypes_to_s).to eq("\t0/0:522:786:522,0\t0/1:459:2930:284,175\t0/0:1046:8258:565,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].chrom).to eq("RAD_contig_8")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].pos).to eq(509)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].id).to eq("test_snps_RAD_contig_8_509")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].qual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].wqual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].info["AS"]).to eq("AAG[T/G]TTG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].genotypes_to_s).to eq("\t0/0:1250:1881:1250,0\t0/1:1227:1847:999,1227\t0/0:1859:2798:1859,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].chrom).to eq("RAD_contig_13")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].pos).to eq(344)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].id).to eq("test_snps_RAD_contig_13_344")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].ref).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].qual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].wqual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].info["AS"]).to eq("GTC[C/G]ATG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].genotypes_to_s).to eq("\t0/0:395:594:395,0\t0/1:268:1514:174,94\t0/0:880:1324:880,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].chrom).to eq("RAD_contig_3")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].pos).to eq(130)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].id).to eq("test_snps_RAD_contig_3_130")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].ref).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].qual).to eq(206)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].wqual).to eq(206)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].info["AS"]).to eq("CGC[C/A]TTG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].genotypes_to_s).to eq("\t0/0:77:116:77,0\t0/1:53:80:53,46\t0/0:48:206:34,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].chrom).to eq("RAD_contig_7")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].pos).to eq(490)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].id).to eq("test_snps_RAD_contig_7_490")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].qual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].wqual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].info["AS"]).to eq("CGC[G/A]TGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].genotypes_to_s).to eq("\t0/0:1326:1996:1326,0\t0/0:1158:9593:611,0\t1/1:1844:2756:0,1843")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].chrom).to eq("RAD_contig_11")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].pos).to eq(286)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].id).to eq("test_snps_RAD_contig_11_286")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].qual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].wqual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].info["AS"]).to eq("TTT[G/A]GTA")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].genotypes_to_s).to eq("\t0/0:178:268:178,0\t0/0:18:27:18,0\t1/1:386:565:0,385")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].chrom).to eq("RAD_contig_2")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].pos).to eq(104)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].id).to eq("test_snps_RAD_contig_2_104")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].qual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].wqual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].info["AS"]).to eq("TTG[A/G]CGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].genotypes_to_s).to eq("\t0/0:75:113:75,0\t0/0:49:213:14,0\t1/1:52:312:0,33")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].chrom).to eq("RAD_contig_4")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].pos).to eq(157)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].id).to eq("test_snps_RAD_contig_4_157")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].qual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].wqual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].info["AS"]).to eq("GGA[A/C]GTG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].genotypes_to_s).to eq("\t0/0:109:164:109,0\t1/1:60:360:0,22\t1/1:137:746:0,90")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].chrom).to eq("RAD_contig_12")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].pos).to eq(305)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].id).to eq("test_snps_RAD_contig_12_305")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].qual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].wqual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].info["AS"]).to eq("ACC[T/A]GAT")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].genotypes_to_s).to eq("\t0/0:266:400:266,0\t1/1:36:54:0,36\t1/1:573:841:0,572")

    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].chrom).to eq("RAD_contig_17")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].pos).to eq(112)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_17_112")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].qual).to eq(160)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].wqual).to eq(160)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].info["AS"]).to eq(".")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].genotypes_to_s).to eq("\t0/1:44:66:44,22\t0/1:38:160:27,11\t0/0:94:141:94,0")

    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].chrom).to eq("RAD_contig_15")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].pos).to eq(318)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_15_318")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].qual).to eq(9186)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].wqual).to eq(9186)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].info["AS"]).to eq(".")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].genotypes_to_s).to eq("\t1/1:639:2394:0,163\t1/1:549:4162:0,305\t0/0:650:2630:476,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].chrom).to eq("RAD_contig_10")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].pos).to eq(273)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].id).to eq("test_snps_RAD_contig_10_273")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].alt).to eq("T,A")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].qual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].wqual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].info["AS"]).to eq("TGG[G/T,A]CCC")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].genotypes_to_s).to eq("\t0/0:196:295:0,0,0\t0/2:18:27:0,18,19\t0/2:360:542:0,360,300")
  end

  it "processes two vcf files" do

    file = Dir.pwd + "/data/testdata/genotypes_100x100.vcf"
    flank_length = 100
    multiplier = 1

    file2 = Dir.pwd + "/data/testdata/genotypes_75x75.vcf"
    flank_length2 = 75
    multiplier2 = 0.75

    prefix = "test_snps"

    vcf1 = VCF.new
    vcf1.read_file(file, "single", flank_length, multiplier)
    vcf2 = VCF.new
    vcf2.read_file(file2, "single", flank_length2, multiplier2)

    vcfs = [vcf1, vcf2]

    mvcf = vcf1.merge_vcfs(vcfs)

    filter = Hash.new()
    mvcf.filter_vcf(filter)
    fvcf = mvcf

    fvcf.sort_and_group_vcf(prefix)
    svcf = fvcf

    expect(svcf.multiplier).to eq(1)
    expect(svcf.flanking_sequence_length).to eq(0)
    expect(svcf.vcf_header.genotypes_to_s).to eq("\tENUTI_74_1_2\tENUTI_107_1_2\tENUTI_97_3a_2")
    expect(svcf.vcf_entries.size).to eq(18)
    expect(svcf.grouped_vcf_entries.size).to eq(7)
    expect(svcf.groups_sorted_by_qual.size).to eq(7)
    expect(svcf.groups_sorted_by_wqual.size).to eq(7)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"].size).to eq(6)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"].size).to eq(4)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"].size).to eq(3)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"].size).to eq(2)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"].size).to eq(1)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"].size).to eq(1)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"].size).to eq(1)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].chrom).to eq("RAD_contig_14")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].pos).to eq(280)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_14_280")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].qual).to eq(500)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].wqual).to eq(375)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].info["AS"]).to eq("GA[T/C]AG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].genotypes_to_s).to eq("\t0/0:40:60:40,0\t1/1:483:727:0,483\t0/0:308:1768:199,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].chrom).to eq("RAD_contig_18")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].pos).to eq(587)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].id).to eq("test_snps_RAD_contig_18_587")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].qual).to eq(465)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].wqual).to eq(348.75)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].info["AS"]).to eq(".")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].genotypes_to_s).to eq("\t0/0:28:42:28,0\t1/1:309:465:0,309\t0/0:305:438:304,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].chrom).to eq("RAD_contig_5")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].pos).to eq(175)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].id).to eq("test_snps_RAD_contig_5_175")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].alt).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].qual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].wqual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].info["AS"]).to eq("CCG[G/T]AGC")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].genotypes_to_s).to eq("\t0/0:145:218:145,0\t1/1:89:134:0,89\t0/0:233:330:232,1")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].chrom).to eq("RAD_contig_9")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].pos).to eq(248)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].id).to eq("test_snps_RAD_contig_9_248")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].qual).to eq(150)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].wqual).to eq(150)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].info["AS"]).to eq("GAG[T/C]CGC")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].genotypes_to_s).to eq("\t0/0:188:283:188,0\t1/1:19:29:0,190\t0/0:280:421:280,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].chrom).to eq("RAD_contig_1")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].pos).to eq(39)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].id).to eq("test_snps_RAD_contig_1_39")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].qual).to eq(106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].wqual).to eq(106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].info["AS"]).to eq("TGC[T/C]AGG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].genotypes_to_s).to eq("\t0/0:52:78:52,0\t1/1:31:47:0,31\t0/0:39:59:39,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].chrom).to eq("RAD_contig_16")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].pos).to eq(604)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].id).to eq("test_snps_RAD_contig_16_604")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].qual).to eq(87)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].wqual).to eq(65.25)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].info["AS"]).to eq(".")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].genotypes_to_s).to eq("\t0/0:20:30:20,0\t1/1:58:87:0,58\t0/0:67:101:67,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].chrom).to eq("RAD_contig_6")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].pos).to eq(304)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_6_304")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].qual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].wqual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].info["AS"]).to eq("GGG[A/G]AAG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].genotypes_to_s).to eq("\t0/0:522:786:522,0\t0/1:459:2930:284,175\t0/0:1046:8258:565,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].chrom).to eq("RAD_contig_8")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].pos).to eq(509)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].id).to eq("test_snps_RAD_contig_8_509")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].qual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].wqual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].info["AS"]).to eq("AAG[T/G]TTG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].genotypes_to_s).to eq("\t0/0:1250:1881:1250,0\t0/1:1227:1847:999,1227\t0/0:1859:2798:1859,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].chrom).to eq("RAD_contig_13")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].pos).to eq(344)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].id).to eq("test_snps_RAD_contig_13_344")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].ref).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].qual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].wqual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].info["AS"]).to eq("GTC[C/G]ATG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].genotypes_to_s).to eq("\t0/0:395:594:395,0\t0/1:268:1514:174,94\t0/0:880:1324:880,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].chrom).to eq("RAD_contig_3")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].pos).to eq(130)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].id).to eq("test_snps_RAD_contig_3_130")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].ref).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].qual).to eq(206)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].wqual).to eq(206)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].info["AS"]).to eq("CGC[C/A]TTG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].genotypes_to_s).to eq("\t0/0:77:116:77,0\t0/1:53:80:53,46\t0/0:48:206:34,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].chrom).to eq("RAD_contig_7")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].pos).to eq(490)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].id).to eq("test_snps_RAD_contig_7_490")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].qual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].wqual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].info["AS"]).to eq("CGC[G/A]TGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].genotypes_to_s).to eq("\t0/0:1326:1996:1326,0\t0/0:1158:9593:611,0\t1/1:1844:2756:0,1843")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].chrom).to eq("RAD_contig_11")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].pos).to eq(286)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].id).to eq("test_snps_RAD_contig_11_286")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].qual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].wqual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].info["AS"]).to eq("TTT[G/A]GTA")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].genotypes_to_s).to eq("\t0/0:178:268:178,0\t0/0:18:27:18,0\t1/1:386:565:0,385")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].chrom).to eq("RAD_contig_2")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].pos).to eq(104)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].id).to eq("test_snps_RAD_contig_2_104")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].qual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].wqual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].info["AS"]).to eq("TTG[A/G]CGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].genotypes_to_s).to eq("\t0/0:75:113:75,0\t0/0:49:213:14,0\t1/1:52:312:0,33")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].chrom).to eq("RAD_contig_4")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].pos).to eq(157)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].id).to eq("test_snps_RAD_contig_4_157")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].qual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].wqual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].info["AS"]).to eq("GGA[A/C]GTG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].genotypes_to_s).to eq("\t0/0:109:164:109,0\t1/1:60:360:0,22\t1/1:137:746:0,90")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].chrom).to eq("RAD_contig_12")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].pos).to eq(305)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].id).to eq("test_snps_RAD_contig_12_305")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].qual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].wqual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].info["AS"]).to eq("ACC[T/A]GAT")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].genotypes_to_s).to eq("\t0/0:266:400:266,0\t1/1:36:54:0,36\t1/1:573:841:0,572")

    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].chrom).to eq("RAD_contig_17")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].pos).to eq(112)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_17_112")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].qual).to eq(160)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].wqual).to eq(120)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].info["AS"]).to eq(".")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].genotypes_to_s).to eq("\t0/1:44:66:44,22\t0/1:38:160:27,11\t0/0:94:141:94,0")

    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].chrom).to eq("RAD_contig_15")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].pos).to eq(318)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_15_318")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].qual).to eq(9186)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].wqual).to eq(6889.5)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].info["AS"]).to eq("TT[G/A]GG")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].genotypes_to_s).to eq("\t1/1:639:2394:0,163\t1/1:549:4162:0,305\t0/0:650:2630:476,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].chrom).to eq("RAD_contig_10")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].pos).to eq(273)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].id).to eq("test_snps_RAD_contig_10_273")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].alt).to eq("T,A")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].qual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].wqual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].info["AS"]).to eq("TGG[G/T,A]CCC")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].genotypes_to_s).to eq("\t0/0:196:295:0,0,0\t0/2:18:27:0,18,19\t0/2:360:542:0,360,300")
  end

  it "processes three vcf files" do
    file = Dir.pwd + "/data/testdata/genotypes_100x100.vcf"
    flank_length = 100
    multiplier = 1

    file2 = Dir.pwd + "/data/testdata/genotypes_75x75.vcf"
    flank_length2 = 75
    multiplier2 = 0.75

    file3 = Dir.pwd + "/data/testdata/genotypes_50x50.vcf"
    flank_length3 = 50
    multiplier3 = 0.5

    prefix = "test_snps"

    vcf1 = VCF.new
    vcf1.read_file(file, "single", flank_length, multiplier)
    vcf2 = VCF.new
    vcf2.read_file(file2, "single", flank_length2, multiplier2)
    vcf3 = VCF.new
    vcf3.read_file(file3, "single", flank_length3, multiplier3)

    vcfs = [vcf1, vcf2, vcf3]

    mvcf = vcf1.merge_vcfs(vcfs)

    filter = Hash.new()
    mvcf.filter_vcf(filter)
    fvcf = mvcf

    fvcf.sort_and_group_vcf(prefix)
    svcf = fvcf

    expect(svcf.multiplier).to eq(1)
    expect(svcf.flanking_sequence_length).to eq(0)
    expect(svcf.vcf_header.genotypes_to_s).to eq("\tENUTI_74_1_2\tENUTI_107_1_2\tENUTI_97_3a_2")
    expect(svcf.vcf_entries.size).to eq(18)
    expect(svcf.grouped_vcf_entries.size).to eq(7)
    expect(svcf.groups_sorted_by_qual.size).to eq(7)
    expect(svcf.groups_sorted_by_wqual.size).to eq(7)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"].size).to eq(6)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"].size).to eq(4)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"].size).to eq(3)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"].size).to eq(2)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"].size).to eq(1)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"].size).to eq(1)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"].size).to eq(1)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].chrom).to eq("RAD_contig_14")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].pos).to eq(280)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_14_280")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].qual).to eq(500)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].wqual).to eq(375)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].info["AS"]).to eq("GA[T/C]AG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].genotypes_to_s).to eq("\t0/0:40:60:40,0\t1/1:483:727:0,483\t0/0:308:1768:199,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].chrom).to eq("RAD_contig_18")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].pos).to eq(587)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].id).to eq("test_snps_RAD_contig_18_587")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].qual).to eq(465)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].wqual).to eq(232.5)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].info["AS"]).to eq("A[T/C]G")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].genotypes_to_s).to eq("\t0/0:28:42:28,0\t1/1:309:465:0,309\t0/0:305:438:304,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].chrom).to eq("RAD_contig_5")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].pos).to eq(175)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].id).to eq("test_snps_RAD_contig_5_175")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].alt).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].qual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].wqual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].info["AS"]).to eq("CCG[G/T]AGC")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].genotypes_to_s).to eq("\t0/0:145:218:145,0\t1/1:89:134:0,89\t0/0:233:330:232,1")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].chrom).to eq("RAD_contig_9")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].pos).to eq(248)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].id).to eq("test_snps_RAD_contig_9_248")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].qual).to eq(150)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].wqual).to eq(150)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].info["AS"]).to eq("GAG[T/C]CGC")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].genotypes_to_s).to eq("\t0/0:188:283:188,0\t1/1:19:29:0,190\t0/0:280:421:280,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].chrom).to eq("RAD_contig_1")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].pos).to eq(39)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].id).to eq("test_snps_RAD_contig_1_39")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].qual).to eq(106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].wqual).to eq(106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].info["AS"]).to eq("TGC[T/C]AGG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].genotypes_to_s).to eq("\t0/0:52:78:52,0\t1/1:31:47:0,31\t0/0:39:59:39,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].chrom).to eq("RAD_contig_16")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].pos).to eq(604)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].id).to eq("test_snps_RAD_contig_16_604")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].qual).to eq(87)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].wqual).to eq(43.5)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].info["AS"]).to eq(".")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][5].genotypes_to_s).to eq("\t0/0:20:30:20,0\t1/1:58:87:0,58\t0/0:67:101:67,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].chrom).to eq("RAD_contig_6")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].pos).to eq(304)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_6_304")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].qual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].wqual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].info["AS"]).to eq("GGG[A/G]AAG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].genotypes_to_s).to eq("\t0/0:522:786:522,0\t0/1:459:2930:284,175\t0/0:1046:8258:565,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].chrom).to eq("RAD_contig_8")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].pos).to eq(509)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].id).to eq("test_snps_RAD_contig_8_509")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].qual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].wqual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].info["AS"]).to eq("AAG[T/G]TTG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].genotypes_to_s).to eq("\t0/0:1250:1881:1250,0\t0/1:1227:1847:999,1227\t0/0:1859:2798:1859,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].chrom).to eq("RAD_contig_13")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].pos).to eq(344)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].id).to eq("test_snps_RAD_contig_13_344")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].ref).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].qual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].wqual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].info["AS"]).to eq("GTC[C/G]ATG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].genotypes_to_s).to eq("\t0/0:395:594:395,0\t0/1:268:1514:174,94\t0/0:880:1324:880,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].chrom).to eq("RAD_contig_3")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].pos).to eq(130)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].id).to eq("test_snps_RAD_contig_3_130")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].ref).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].qual).to eq(206)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].wqual).to eq(206)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].info["AS"]).to eq("CGC[C/A]TTG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].genotypes_to_s).to eq("\t0/0:77:116:77,0\t0/1:53:80:53,46\t0/0:48:206:34,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].chrom).to eq("RAD_contig_7")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].pos).to eq(490)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].id).to eq("test_snps_RAD_contig_7_490")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].qual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].wqual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].info["AS"]).to eq("CGC[G/A]TGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].genotypes_to_s).to eq("\t0/0:1326:1996:1326,0\t0/0:1158:9593:611,0\t1/1:1844:2756:0,1843")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].chrom).to eq("RAD_contig_11")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].pos).to eq(286)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].id).to eq("test_snps_RAD_contig_11_286")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].qual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].wqual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].info["AS"]).to eq("TTT[G/A]GTA")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].genotypes_to_s).to eq("\t0/0:178:268:178,0\t0/0:18:27:18,0\t1/1:386:565:0,385")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].chrom).to eq("RAD_contig_2")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].pos).to eq(104)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].id).to eq("test_snps_RAD_contig_2_104")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].qual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].wqual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].info["AS"]).to eq("TTG[A/G]CGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].genotypes_to_s).to eq("\t0/0:75:113:75,0\t0/0:49:213:14,0\t1/1:52:312:0,33")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].chrom).to eq("RAD_contig_4")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].pos).to eq(157)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].id).to eq("test_snps_RAD_contig_4_157")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].qual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].wqual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].info["AS"]).to eq("GGA[A/C]GTG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].genotypes_to_s).to eq("\t0/0:109:164:109,0\t1/1:60:360:0,22\t1/1:137:746:0,90")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].chrom).to eq("RAD_contig_12")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].pos).to eq(305)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].id).to eq("test_snps_RAD_contig_12_305")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].qual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].wqual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].info["AS"]).to eq("ACC[T/A]GAT")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].genotypes_to_s).to eq("\t0/0:266:400:266,0\t1/1:36:54:0,36\t1/1:573:841:0,572")

    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].chrom).to eq("RAD_contig_17")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].pos).to eq(112)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_17_112")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].qual).to eq(160)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].wqual).to eq(80)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].info["AS"]).to eq("G[T/A]C")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].genotypes_to_s).to eq("\t0/1:44:66:44,22\t0/1:38:160:27,11\t0/0:94:141:94,0")

    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].chrom).to eq("RAD_contig_15")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].pos).to eq(318)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_15_318")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].qual).to eq(9186)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].wqual).to eq(6889.5)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].info["AS"]).to eq("TT[G/A]GG")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].genotypes_to_s).to eq("\t1/1:639:2394:0,163\t1/1:549:4162:0,305\t0/0:650:2630:476,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].chrom).to eq("RAD_contig_10")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].pos).to eq(273)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].id).to eq("test_snps_RAD_contig_10_273")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].alt).to eq("T,A")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].qual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].wqual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].info["AS"]).to eq("TGG[G/T,A]CCC")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].genotypes_to_s).to eq("\t0/0:196:295:0,0,0\t0/2:18:27:0,18,19\t0/2:360:542:0,360,300")
  end

  it "processes three vcf files and filters flanks" do
    file = Dir.pwd + "/data/testdata/genotypes_100x100.vcf"
    flank_length = 100
    multiplier = 1

    file2 = Dir.pwd + "/data/testdata/genotypes_75x75.vcf"
    flank_length2 = 75
    multiplier2 = 0.75

    file3 = Dir.pwd + "/data/testdata/genotypes_50x50.vcf"
    flank_length3 = 50
    multiplier3 = 0.5

    prefix = "test_snps"

    vcf1 = VCF.new
    vcf1.read_file(file, "single", flank_length, multiplier)
    vcf2 = VCF.new
    vcf2.read_file(file2, "single", flank_length2, multiplier2)
    vcf3 = VCF.new
    vcf3.read_file(file3, "single", flank_length3, multiplier3)

    vcfs = [vcf1, vcf2, vcf3]

    mvcf = vcf1.merge_vcfs(vcfs)

    filter = Hash.new()
    filter.store("discard_flank", 1)
    mvcf.filter_vcf(filter)
    fvcf = mvcf

    fvcf.sort_and_group_vcf(prefix)
    svcf = fvcf

    expect(svcf.multiplier).to eq(1)
    expect(svcf.flanking_sequence_length).to eq(0)
    expect(svcf.vcf_header.genotypes_to_s).to eq("\tENUTI_74_1_2\tENUTI_107_1_2\tENUTI_97_3a_2")
    expect(svcf.vcf_entries.size).to eq(17)
    expect(svcf.grouped_vcf_entries.size).to eq(7)
    expect(svcf.groups_sorted_by_qual.size).to eq(7)
    expect(svcf.groups_sorted_by_wqual.size).to eq(7)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"].size).to eq(5)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"].size).to eq(4)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"].size).to eq(3)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"].size).to eq(2)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"].size).to eq(1)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"].size).to eq(1)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"].size).to eq(1)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].chrom).to eq("RAD_contig_14")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].pos).to eq(280)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_14_280")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].qual).to eq(500)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].wqual).to eq(375)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].info["AS"]).to eq("GA[T/C]AG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].genotypes_to_s).to eq("\t0/0:40:60:40,0\t1/1:483:727:0,483\t0/0:308:1768:199,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].chrom).to eq("RAD_contig_18")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].pos).to eq(587)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].id).to eq("test_snps_RAD_contig_18_587")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].qual).to eq(465)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].wqual).to eq(232.5)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].info["AS"]).to eq("A[T/C]G")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].genotypes_to_s).to eq("\t0/0:28:42:28,0\t1/1:309:465:0,309\t0/0:305:438:304,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].chrom).to eq("RAD_contig_5")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].pos).to eq(175)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].id).to eq("test_snps_RAD_contig_5_175")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].alt).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].qual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].wqual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].info["AS"]).to eq("CCG[G/T]AGC")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].genotypes_to_s).to eq("\t0/0:145:218:145,0\t1/1:89:134:0,89\t0/0:233:330:232,1")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].chrom).to eq("RAD_contig_9")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].pos).to eq(248)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].id).to eq("test_snps_RAD_contig_9_248")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].qual).to eq(150)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].wqual).to eq(150)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].info["AS"]).to eq("GAG[T/C]CGC")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][3].genotypes_to_s).to eq("\t0/0:188:283:188,0\t1/1:19:29:0,190\t0/0:280:421:280,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].chrom).to eq("RAD_contig_1")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].pos).to eq(39)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].id).to eq("test_snps_RAD_contig_1_39")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].qual).to eq(106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].wqual).to eq(106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].info["AS"]).to eq("TGC[T/C]AGG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][4].genotypes_to_s).to eq("\t0/0:52:78:52,0\t1/1:31:47:0,31\t0/0:39:59:39,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].chrom).to eq("RAD_contig_6")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].pos).to eq(304)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_6_304")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].qual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].wqual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].info["AS"]).to eq("GGG[A/G]AAG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].genotypes_to_s).to eq("\t0/0:522:786:522,0\t0/1:459:2930:284,175\t0/0:1046:8258:565,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].chrom).to eq("RAD_contig_8")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].pos).to eq(509)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].id).to eq("test_snps_RAD_contig_8_509")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].qual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].wqual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].info["AS"]).to eq("AAG[T/G]TTG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].genotypes_to_s).to eq("\t0/0:1250:1881:1250,0\t0/1:1227:1847:999,1227\t0/0:1859:2798:1859,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].chrom).to eq("RAD_contig_13")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].pos).to eq(344)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].id).to eq("test_snps_RAD_contig_13_344")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].ref).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].qual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].wqual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].info["AS"]).to eq("GTC[C/G]ATG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].genotypes_to_s).to eq("\t0/0:395:594:395,0\t0/1:268:1514:174,94\t0/0:880:1324:880,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].chrom).to eq("RAD_contig_3")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].pos).to eq(130)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].id).to eq("test_snps_RAD_contig_3_130")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].ref).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].qual).to eq(206)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].wqual).to eq(206)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].info["AS"]).to eq("CGC[C/A]TTG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][3].genotypes_to_s).to eq("\t0/0:77:116:77,0\t0/1:53:80:53,46\t0/0:48:206:34,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].chrom).to eq("RAD_contig_7")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].pos).to eq(490)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].id).to eq("test_snps_RAD_contig_7_490")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].qual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].wqual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].info["AS"]).to eq("CGC[G/A]TGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].genotypes_to_s).to eq("\t0/0:1326:1996:1326,0\t0/0:1158:9593:611,0\t1/1:1844:2756:0,1843")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].chrom).to eq("RAD_contig_11")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].pos).to eq(286)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].id).to eq("test_snps_RAD_contig_11_286")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].qual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].wqual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].info["AS"]).to eq("TTT[G/A]GTA")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].genotypes_to_s).to eq("\t0/0:178:268:178,0\t0/0:18:27:18,0\t1/1:386:565:0,385")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].chrom).to eq("RAD_contig_2")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].pos).to eq(104)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].id).to eq("test_snps_RAD_contig_2_104")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].qual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].wqual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].info["AS"]).to eq("TTG[A/G]CGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].genotypes_to_s).to eq("\t0/0:75:113:75,0\t0/0:49:213:14,0\t1/1:52:312:0,33")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].chrom).to eq("RAD_contig_4")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].pos).to eq(157)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].id).to eq("test_snps_RAD_contig_4_157")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].qual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].wqual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].info["AS"]).to eq("GGA[A/C]GTG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].genotypes_to_s).to eq("\t0/0:109:164:109,0\t1/1:60:360:0,22\t1/1:137:746:0,90")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].chrom).to eq("RAD_contig_12")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].pos).to eq(305)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].id).to eq("test_snps_RAD_contig_12_305")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].qual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].wqual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].info["AS"]).to eq("ACC[T/A]GAT")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].genotypes_to_s).to eq("\t0/0:266:400:266,0\t1/1:36:54:0,36\t1/1:573:841:0,572")

    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].chrom).to eq("RAD_contig_17")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].pos).to eq(112)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_17_112")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].qual).to eq(160)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].wqual).to eq(80)
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].info["AS"]).to eq("G[T/A]C")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/1:0/1:0/0"][0].genotypes_to_s).to eq("\t0/1:44:66:44,22\t0/1:38:160:27,11\t0/0:94:141:94,0")

    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].chrom).to eq("RAD_contig_15")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].pos).to eq(318)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_15_318")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].qual).to eq(9186)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].wqual).to eq(6889.5)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].info["AS"]).to eq("TT[G/A]GG")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].genotypes_to_s).to eq("\t1/1:639:2394:0,163\t1/1:549:4162:0,305\t0/0:650:2630:476,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].chrom).to eq("RAD_contig_10")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].pos).to eq(273)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].id).to eq("test_snps_RAD_contig_10_273")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].alt).to eq("T,A")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].qual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].wqual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].info["AS"]).to eq("TGG[G/T,A]CCC")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].genotypes_to_s).to eq("\t0/0:196:295:0,0,0\t0/2:18:27:0,18,19\t0/2:360:542:0,360,300")
  end

  it "processes three vcf files and filters flanks and quality" do
    file = Dir.pwd + "/data/testdata/genotypes_100x100.vcf"
    flank_length = 100
    multiplier = 1

    file2 = Dir.pwd + "/data/testdata/genotypes_75x75.vcf"
    flank_length2 = 75
    multiplier2 = 0.75

    file3 = Dir.pwd + "/data/testdata/genotypes_50x50.vcf"
    flank_length3 = 50
    multiplier3 = 0.5

    prefix = "test_snps"

    vcf1 = VCF.new
    vcf1.read_file(file, "single", flank_length, multiplier)
    vcf2 = VCF.new
    vcf2.read_file(file2, "single", flank_length2, multiplier2)
    vcf3 = VCF.new
    vcf3.read_file(file3, "single", flank_length3, multiplier3)

    vcfs = [vcf1, vcf2, vcf3]

    mvcf = vcf1.merge_vcfs(vcfs)

    filter = Hash.new()
    filter.store("discard_flank", 1)
    filter.store("qual", 300)
    mvcf.filter_vcf(filter)
    fvcf = mvcf

    fvcf.sort_and_group_vcf(prefix)
    svcf = fvcf

    expect(svcf.multiplier).to eq(1)
    expect(svcf.flanking_sequence_length).to eq(0)
    expect(svcf.vcf_header.genotypes_to_s).to eq("\tENUTI_74_1_2\tENUTI_107_1_2\tENUTI_97_3a_2")
    expect(svcf.vcf_entries.size).to eq(13)
    expect(svcf.grouped_vcf_entries.size).to eq(6)
    expect(svcf.groups_sorted_by_qual.size).to eq(6)
    expect(svcf.groups_sorted_by_wqual.size).to eq(6)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"].size).to eq(3)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"].size).to eq(3)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"].size).to eq(3)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"].size).to eq(2)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"].size).to eq(1)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"].size).to eq(1)

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].chrom).to eq("RAD_contig_14")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].pos).to eq(280)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_14_280")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].qual).to eq(500)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].wqual).to eq(375)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].info["AS"]).to eq("GA[T/C]AG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][0].genotypes_to_s).to eq("\t0/0:40:60:40,0\t1/1:483:727:0,483\t0/0:308:1768:199,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].chrom).to eq("RAD_contig_18")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].pos).to eq(587)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].id).to eq("test_snps_RAD_contig_18_587")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].qual).to eq(465)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].wqual).to eq(232.5)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].info["AS"]).to eq("A[T/C]G")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][1].genotypes_to_s).to eq("\t0/0:28:42:28,0\t1/1:309:465:0,309\t0/0:305:438:304,0")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].chrom).to eq("RAD_contig_5")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].pos).to eq(175)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].id).to eq("test_snps_RAD_contig_5_175")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].alt).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].qual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].wqual).to eq(464)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].info["AS"]).to eq("CCG[G/T]AGC")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:0/0"][2].genotypes_to_s).to eq("\t0/0:145:218:145,0\t1/1:89:134:0,89\t0/0:233:330:232,1")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].chrom).to eq("RAD_contig_6")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].pos).to eq(304)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_6_304")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].qual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].wqual).to eq(11188)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].info["AS"]).to eq("GGG[A/G]AAG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][0].genotypes_to_s).to eq("\t0/0:522:786:522,0\t0/1:459:2930:284,175\t0/0:1046:8258:565,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].chrom).to eq("RAD_contig_8")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].pos).to eq(509)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].id).to eq("test_snps_RAD_contig_8_509")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].qual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].wqual).to eq(4645)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].info["AS"]).to eq("AAG[T/G]TTG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][1].genotypes_to_s).to eq("\t0/0:1250:1881:1250,0\t0/1:1227:1847:999,1227\t0/0:1859:2798:1859,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].chrom).to eq("RAD_contig_13")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].pos).to eq(344)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].id).to eq("test_snps_RAD_contig_13_344")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].ref).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].qual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].wqual).to eq(1514)
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].info["AS"]).to eq("GTC[C/G]ATG")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/1:0/0"][2].genotypes_to_s).to eq("\t0/0:395:594:395,0\t0/1:268:1514:174,94\t0/0:880:1324:880,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].chrom).to eq("RAD_contig_7")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].pos).to eq(490)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].id).to eq("test_snps_RAD_contig_7_490")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].qual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].wqual).to eq(9593)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].info["AS"]).to eq("CGC[G/A]TGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][0].genotypes_to_s).to eq("\t0/0:1326:1996:1326,0\t0/0:1158:9593:611,0\t1/1:1844:2756:0,1843")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].chrom).to eq("RAD_contig_11")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].pos).to eq(286)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].id).to eq("test_snps_RAD_contig_11_286")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].qual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].wqual).to eq(592)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].info["AS"]).to eq("TTT[G/A]GTA")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][1].genotypes_to_s).to eq("\t0/0:178:268:178,0\t0/0:18:27:18,0\t1/1:386:565:0,385")

    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].chrom).to eq("RAD_contig_2")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].pos).to eq(104)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].id).to eq("test_snps_RAD_contig_2_104")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].alt).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].qual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].wqual).to eq(525)
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].info["AS"]).to eq("TTG[A/G]CGT")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/0:1/1"][2].genotypes_to_s).to eq("\t0/0:75:113:75,0\t0/0:49:213:14,0\t1/1:52:312:0,33")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].chrom).to eq("RAD_contig_4")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].pos).to eq(157)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].id).to eq("test_snps_RAD_contig_4_157")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].ref).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].alt).to eq("C")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].qual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].wqual).to eq(1106)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].info["AS"]).to eq("GGA[A/C]GTG")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][0].genotypes_to_s).to eq("\t0/0:109:164:109,0\t1/1:60:360:0,22\t1/1:137:746:0,90")

    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].chrom).to eq("RAD_contig_12")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].pos).to eq(305)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].id).to eq("test_snps_RAD_contig_12_305")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].ref).to eq("T")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].qual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].wqual).to eq(895)
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].info["AS"]).to eq("ACC[T/A]GAT")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:1/1:1/1"][1].genotypes_to_s).to eq("\t0/0:266:400:266,0\t1/1:36:54:0,36\t1/1:573:841:0,572")

    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].chrom).to eq("RAD_contig_15")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].pos).to eq(318)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].id).to eq("test_snps_RAD_contig_15_318")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].alt).to eq("A")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].qual).to eq(9186)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].wqual).to eq(6889.5)
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].info["AS"]).to eq("TT[G/A]GG")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["1/1:1/1:0/0"][0].genotypes_to_s).to eq("\t1/1:639:2394:0,163\t1/1:549:4162:0,305\t0/0:650:2630:476,0")

    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].chrom).to eq("RAD_contig_10")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].pos).to eq(273)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].id).to eq("test_snps_RAD_contig_10_273")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].ref).to eq("G")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].alt).to eq("T,A")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].qual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].wqual).to eq(569)
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].filter).to eq("PASS")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].info["AS"]).to eq("TGG[G/T,A]CCC")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.groups_sorted_by_qual["0/0:0/2:0/2"][0].genotypes_to_s).to eq("\t0/0:196:295:0,0,0\t0/2:18:27:0,18,19\t0/2:360:542:0,360,300")
  end

  it "processes vcf file and groups samples" do
    file = Dir.pwd + "/data/testdata/pop_test.vcf"
    flank_length = 100
    multiplier = 1

    groups = { "Pop1" => ["HUMUS_72_1", "HUMUS_92_3", "HUMUS_112_2"], "Pop2" => ["HUMUS_101_1", "HUMUS_57_1", "HUMUS_23_1"]}

    prefix = "test_snps"

    vcf1 = VCF.new
    vcf1.read_file(file, "single", flank_length, multiplier)

    vcfs = [vcf1]

    mvcf = vcf1.merge_vcfs(vcfs)

    filter = Hash.new()
    mvcf.filter_vcf(filter)
    fvcf = mvcf

    fvcf.condense_groups(groups)

    fvcf.sort_and_group_vcf(prefix)
    svcf = fvcf

    expect(svcf.multiplier).to eq(1)
    expect(svcf.flanking_sequence_length).to eq(0)
    expect(svcf.vcf_header.condensed_genotypes[0]).to eq("Pop1")
    expect(svcf.vcf_header.condensed_genotypes[1]).to eq("Pop2")
    expect(svcf.vcf_entries.size).to eq(3)

    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].chrom).to eq("RAD_contig_1")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].pos).to eq(39)
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].id).to eq("test_snps_RAD_contig_1_39")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].ref).to eq("T")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].alt).to eq("C")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].qual).to eq(106)
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].wqual).to eq(106)
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].filter).to eq("PASS")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].info["AS"]).to eq("TGC[T/C]AGG")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].condensed_genotypes[0]).to eq("2:4")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][0].condensed_genotypes[1]).to eq("4:2")

    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].chrom).to eq("RAD_contig_2")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].pos).to eq(104)
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].id).to eq("test_snps_RAD_contig_2_104")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].ref).to eq("A")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].alt).to eq("G")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].qual).to eq(525)
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].wqual).to eq(525)
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].filter).to eq("PASS")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].info["AS"]).to eq("TTG[A/G]CGT")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].format).to eq("GT:DP:GQ:AD")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].condensed_genotypes[0]).to eq("2:4")
    expect(svcf.grouped_vcf_entries["1/1:1/1:0/1:0/0:0/1:0/0"][1].condensed_genotypes[1]).to eq("4:2")

    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].chrom).to eq("RAD_contig_3")
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].pos).to eq(130)
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].id).to eq("test_snps_RAD_contig_3_130")
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].ref).to eq("C")
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].alt).to eq("A")
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].qual).to eq(206)
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].wqual).to eq(206)
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].filter).to eq("PASS")
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].info["AS"]).to eq("CGC[C/A]TTG")
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].format).to eq("GT:DP:GQ:AD")
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].condensed_genotypes[0]).to eq("4:2")
    expect(svcf.grouped_vcf_entries["0/0:0/1:0/0:0/1:1/1:0/1"][0].condensed_genotypes[1]).to eq("3:3")

  end

end