#!/usr/bin/env ruby

=begin
  Copyright (c) 2019 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2019 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require_relative 'lib/vcf'
require_relative 'lib/vcf_entry'
require_relative 'lib/get_flanking_sequences_option_parser.rb'
require_relative 'lib/plotter'

require 'byebug'

options = OptionParser.parse(ARGV)

vcf_file = options.vcf_file
fasta_file = options.fasta_file
flank_length = options.flank_length
maximum_snps_allowed = options.maximum_snps_allowed
maximum_snps_freq_allowed = options.maximum_snps_freq_allowed

vcf = VCF.new
vcf.read_file(vcf_file, "single", flank_length, 1)
vcf.read_fasta(fasta_file)

vcf.calculate_flanking_sequence(maximum_snps_allowed,
                                maximum_snps_freq_allowed,
                                options.iupac_snps)

filter = Hash.new()
if(options.filter_flank)
  filter.store("discard_flank", 1)
end

vcf.filter_vcf(filter)

plotter = Plotter.new

plotter.write_vcf(vcf, options.output)