#!/usr/bin/env ruby

=begin
  Copyright (c) 2019 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2019 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require_relative 'lib/vcf'
require_relative 'lib/vcf_entry'
require_relative 'lib/vcf2gdant_option_parser.rb'

require 'byebug'

def genotypes2allel_codes(entry, allele, allele_codes)
  code = ""
  if (allele.eql?("0"))
    code = entry.ref
  end
  if (allele.eql?("."))
    code = "undefined"
  end
  if (allele.to_i > 0)
    code = entry.alt.split(",")[allele.to_i - 1]
  end

  if(allele_codes.key?(code))
    ac = allele_codes[code]
  else
    puts("Allele #{code} is not defined. '-1' will be used.")
    puts("Check in your allele_codes config file if #{code} is correctly defined.")
    ac = -1
  end

  return ac
end

options = OptionParser.parse(ARGV)

vcf_file = options.vcf_file

group_file = options.group_file

coords_file = options.coords_file

allele_codes_file = options.allele_codes

title = options.title

prefix = options.prefix

highest_allel_code = options.highest_allel_code

coord = [1,0]

vcf = VCF.new
vcf.read_file(vcf_file, "single", 1, 1)

if(options.freq_filter != 0)
  vcf.filter_allele_frequency(vcf.vcf_entries,options.freq_filter)
end

filter = Hash.new()
if(options.filter_flank)
  filter.store("discard_flank", 1)
end
if(options.filter_qual > 0)
  filter.store("qual", options.filter_qual)
end

vcf.filter_vcf(filter)

number_of_allels = vcf.vcf_entries.length

allele_codes = Hash.new
File.open(allele_codes_file, "r") do |f|
  f.each_line do |line|
    sline = line.chomp.split("\t")
    if(allele_codes.has_key?(sline[0]))
      allele_codes[sline[0]] = sline[1].to_i
    else
      allele_codes.store(sline[0], sline[1].to_i)
    end
  end
end

groups = Hash.new
File.open(group_file, "r") do |f|
  f.each_line do |line|
    sline = line.chomp.split("\t")
    if(groups.has_key?(sline[0]))
      groups[sline[0]].push(sline[1])
    else
      groups.store(sline[0], [sline[1]])
    end
  end
end

group_coords = Hash.new
if (!coords_file.eql?(""))
  File.open(coords_file, "r") do |f|
    f.each_line do |line|
      sline = line.chomp.split("\t")
      if(group_coords.has_key?(sline[0]))
        group_coords[sline[0]].push(sline[1])
      else
        group_coords.store(sline[0], [sline[1], sline[2]])
      end
    end
  end
end

number_of_groups = groups.length

open(options.output, 'w') do |f|
  f.puts title
  f.puts number_of_groups
  f.puts number_of_allels
  f.puts highest_allel_code
  vcf.vcf_entries.each do |k,entry|
    suffix = entry.chrom + "_" + entry.pos.to_s
    if(!prefix.eql?(""))
      entry.id = prefix + "_" + suffix
    else 
      entry.id = suffix
    end
    f.puts entry.id
  end

  ci = 0
  groups_by_alleles = Hash.new

  groups.each do |pop_name,samples_arr|

    samples_and_alleles = Hash.new

    samples_arr.each do |sample_name|
      index = vcf.vcf_header.genotypes.index(sample_name)
      vcf.vcf_entries.each do |k,entry|
        gt = entry.genotypes[index]
        gt_split = gt.split(":")[0]
        if (gt_split.eql?("."))
          alleles = [".","."]
        else
          if(gt_split.include?("/"))
            alleles = gt_split.split("/")
          else
            alleles = [gt_split, gt_split]
          end
        end
        a1 = genotypes2allel_codes(entry, alleles[0], allele_codes).to_s
        a2 = genotypes2allel_codes(entry, alleles[1], allele_codes).to_s
        if(a1.eql?("-1"))
          a2 = "-1"
        end
        if(a2.eql?("-1"))
          a1 = "-1"
        end
        gdant_gt = a1 + "," + a2
        if(!samples_and_alleles.key?(sample_name))
          samples_and_alleles.store(sample_name, gdant_gt)
        else
          samples_and_alleles[sample_name] += "," + gdant_gt
        end
      end
    end

    groups_by_alleles.store(pop_name, samples_and_alleles)

  end

  groups_by_alleles.each_with_index do |(pop_name, samples_and_alleles), index|

    if (group_coords.empty?())
      f.puts pop_name + "," + (coord[0] + ci).to_s + "," + coord[1].to_s
      ci += 1
    else
      f.puts pop_name + "," + group_coords[pop_name][0] + "," + group_coords[pop_name][1]
    end

    samples_and_alleles.each do |k, alleles|
      f.puts alleles
    end

    if (index != (groups_by_alleles.length - 1))
      f.puts ""
    end

  end

  f.puts "END"
end