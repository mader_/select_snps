Select SNPs
-----------

This is a collection of scripts for SNP selection from VCF files.

INSTALL
-------

The scripts need a working ruby installation. They have been tested with ruby
2.6.0. To install denpendencies bundler ist needed: "gem install bundler".
Dependencies can then be installed with "bundle install".

Scripts
-------

Following scripts are included:

select_snps.rb -- sort filter and visualize VCF data

vcf2gdant.rb -- convert genotype data in VCF to GDANT format

get_flanking_sequences.rb -- calculate flanking sequences for SNPs

vcf2massarray.rb -- converts VCF to massarray format

massarray2vcf.rb -- convert massarray format to VCF

compare_genotypes.rb -- compares variants of VCF files by calculating genetic distances

get_read_frequencies.rb -- extracts allele frequencies from read counts in VCF files

get_stats.rb -- extracts histograms of read frequencies and variant calls from VCF file (experimental)

recall_variants.rb -- adjusts variant calls based on given allele frequency thresholds (experimental)

More detailed documentation and examples can be found in the doc/ directory.
Just type "make" in the doc/ folder to produce the PDF (latex needs to be
installed). "make clean" will remove the PDF and all auxiliary files.

Contact: malte.mader@thuenen.de