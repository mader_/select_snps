\documentclass[a4paper, 12pt, DIV=1]{scrartcl}

\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{cite}
\usepackage{courier}
\usepackage{dirtree}
\usepackage{fancyvrb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[htt]{hyphenat}
\usepackage{listings}
\usepackage{rotating}
\usepackage{tabularx}
\usepackage{times}
\usepackage{url}
\usepackage{xcolor}
\usepackage{xspace}

\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt}

\newcounter{commentcounter}
\newcommand{\Comment}[1]{%
  \par%
  \noindent%
  \fbox{%
  \begin{minipage}{0.98\textwidth}
    \textsl{Comment \#\refstepcounter{commentcounter}%
            \arabic{commentcounter}: #1}%
  \end{minipage}%
  }%
  \par%
}

\lstset{
  basicstyle=\ttfamily,
  columns=fullflexible,
  frame=single,
  breaklines=true,
  postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space},
}

\title{A collection of scripts for the selection of SNPs for genetic marker
       development}

\author{Malte Mader}

\date{02.02.2021}

\begin{document}

\maketitle

\tableofcontents

\pagebreak

\section{Installation and directory structure}

The scripts have been tested with ruby version 2.6.0
(RVM~\url{https://rvm.io/}). To install all dependencies, first install
Bundler with 'gem install bundler' then type 'bundle install' in the console.

The directory structure is as follows:

\dirtree{%
.1 select\_snps/.
.2 data/.
.3 expected\_results/ \DTcomment{ \begin{minipage}[t]{7cm}
                                    Holds manually created text files that store
                                    results which are expected to be generated
                                    by the scripts{.}
                                  \end{minipage}}.
.3 testdata/ \DTcomment{ \begin{minipage}[t]{7cm}
                           Holds text files that store example data for
                           processing{.}
                         \end{minipage}}.
.2 doc/ \DTcomment{ \begin{minipage}[t]{7cm}
                       Contains this documentation{.}
                    \end{minipage}}.
.2 example\_configs/ \DTcomment{ \begin{minipage}[t]{7cm}
                                   Contains text files that specify file
                                   locations or group definitions e.g. of
                                   populations{.}
                                 \end{minipage}}.
.2 lib/ \DTcomment{ \begin{minipage}[t]{7cm}
                      Contains ruby classes used by the scripts{.}
                    \end{minipage}}.
.2 spec/ \DTcomment{ \begin{minipage}[t]{7cm}
                      Contains ruby test classes for testing with Rspec{.}
                    \end{minipage}}.
.2 file\_tests.sh \DTcomment{ \begin{minipage}[t]{7cm}
                                Shell script generating results with different
                                parameters based on the scripts and compare 
                                resulting files with the data from
                                \texttt{expected\_results/}{.}
                              \end{minipage}}.
.2 select\_snps.rb \DTcomment{ \begin{minipage}[t]{7cm}
                                Script for filtering, sorting and visualization
                                of SNPs in VCF{.}
                              \end{minipage}}.
.2 vcf2gdant.rb \DTcomment{ \begin{minipage}[t]{7cm}
                              Script for the conversion of VCF into the GDANT
                              format{.}
                            \end{minipage}}.
.2 get\_flanking\_sequences.rb \DTcomment{ \begin{minipage}[t]{6cm}
                              Script for calculating flanking sequences for
                              SNPs{.}
                            \end{minipage}}.
.2 massarray2vcf.rb \DTcomment{ \begin{minipage}[t]{7cm}
                              Script for converting MASSArray format into VCF{.}
                            \end{minipage}}.
.2 vcf2massarray.rb \DTcomment{ \begin{minipage}[t]{7cm}
                              Script for converting VCF into MASSArray format{.}
                            \end{minipage}}.
.2 compare\_genotypes.rb \DTcomment{ \begin{minipage}[t]{7cm}
                              Script for calculating distance values between
                              genotypes of two datasets{.}
                            \end{minipage}}.
.2 get\_read\_frequencies.rb \DTcomment{ \begin{minipage}[t]{6cm}
                              Script for extracting allele frequencies from
                              read counts in VCF file{.}
                            \end{minipage}}.
}

\section{Select SNPs}

The script \texttt{select\_snps.rb} filters, sorts and visualizes VCF data in
different ways. It takes one or more VCF files as input, specified as a file
list in a text file (option -F). This tab separated text file also includes the
flanking sequence length of each SNP if present and a multiplier value in the
interval of 0 and 1. Multiple VCF files will be merged in one output table.
Shorter flanking sequences (denoted in the INFO field by ``AS='') will be
discarded if multiple SNPs with different flanking length exist. The SNPs can
be filtered for different attributes i.e. all SNPs without flanking sequences
can be omitted (option -f), all SNPs below a defined quality threshold value
can be filtered out (option -q) and all SNPs showing the same genotype in
(almost) all samples can be deleted (option -{}-filter-gt-frequency). For every
SNP an ID will automatically be created consisting of the \#CHROM name and
the position. A custom prefix can be added to the ID (option -p). Samples can
be assigned to distinct groups for which group wise allele counts are
calculated (option -g). Groups are defined in a tab separated file consisting
of two columns (group name and sample name). Custom output file names can be
specified with the option -o.

The resulting MS Excel file contains up to four spreadsheets. The first sheet
contains statistics about all detected genotype groups. The second sheet
contains a (filtered) table of all SNPs and its attributes sorted by the
genotype groups and quality values in descending order. Genotype groups and the
different genotypes of samples are colored for better distinction. The third
sheet contains a (filtered) table of all SNPs and its attributes sorted by the
genotype groups and weighted quality values in descending order. Weighted
quality values are calculated by multiplying the quality value with the
corresponding multiplier provided with the file list. The fourth sheet can
optionally be created if the samples in the VCF file(s) are separated into
groups. This sheet contains allele counts for every defined group instead of
genotypes for every individual sample.

\subsubsection*{Examples}

The easiest possible program call is :
\begin{lstlisting}[language=bash]
$ ./select_snps.rb -F example_configs/example_file_list.txt
\end{lstlisting}

It just merges and sorts the VCF files specified in
\path{example_configs/example_file_list.txt} :

\begin{Verbatim}[frame=single]
#path_to_file flank_length  multiplier
data/testdata/genotypes_50x50.vcf 50  0.5
data/testdata/genotypes_75x75.vcf 75  0.75
data/testdata/genotypes_100x100.vcf 100 
\end{Verbatim}

Generate additional sheet with allele summaries for groups and set prefix of
the ID to \textit{MYSPE} :
\begin{lstlisting}[language=bash]
./select_snps.rb -F example_configs/example_file_list2.txt -g example_configs/example_groups.txt -p MYSPE
\end{lstlisting}

Two groups are defined in \path{example\_configs/example\_groups.txt} :

\begin{Verbatim}[frame=single]
Pop1  HUMUS_72_1
Pop1  HUMUS_92_3
Pop1  HUMUS_112_2
Pop2  HUMUS_101_1
Pop2  HUMUS_57_1
Pop2  HUMUS_23_1
\end{Verbatim}

Remove all SNPs without flanking sequences and with a quality value less than
200. SNPs where more than 80\% of individuals show the genotype ``1/1'', ``0/0''
or ``2/2'' are also removed:

\begin{lstlisting}[language=bash]
./select_snps.rb -F example_configs/example_file_list.txt -f -q 200 --filter-gt-frequency 0.8
\end{lstlisting}

\section{VCF2GDANT}

The script \texttt{vcf2gdant.rb} converts a VCF file into a GDANT file for
statistical analysis. A path to a VCf file (option -f) and a title (option -t)
as well as samples that have been assigned to groups (option -g) must be
provided. An ID is created for each SNP consisting of the contig name and the
SNPs position. A custom prefix can be appended (option -p). The highest allele
count is set to 10 by default which can be changed if necessary (option -a). If
a coordinates file is provided (option -c) geographic coordinates will be
assigned to each population. Otherwise placeholder coordinates will be
generated. Custom allele codes can be defined in a text file (option
-{}-allele\_codes).
Data can be filtered for missing flanking sequences (option -{}-filter\_flanks),
quality values (option -{}-quality\_filter) and genotype frequencies over all
samples (option -{}-filter-gt-frequency). Custom output file names can be
specified with the option -o.

If the output file is created on a Unix-like system, it is important to convert
the resulting text file into MS DOS format. Otherwise GDANT will not be able to
read the file. Text files can be converted with the Unix command line tool
\textit{unix2dos}.

\subsubsection*{Examples}

Minimal example which just converts one VCF file into a GDANT file. Only one
sample group is defined in the group file:

\begin{lstlisting}[language=bash]
./vcf2gdant.rb -f data/testdata/genotypes_50x50.vcf -t test1 -g example_configs/example_groups_one_pop.txt
\end{lstlisting}

Filters all SNPs where more than 90\% of individuals share the same genotype.
SNPs id are prefixed with the string ``SPECI''. Maximum allele count is set to
10:

\begin{lstlisting}[language=bash]
./vcf2gdant.rb -f data/testdata/pop_test_filter.vcf -t pop_test_filter -g example_configs/example_groups.txt -p SPECI -a 10 --filter-gt-frequency 0.9
\end{lstlisting}

\section{get flanking sequences}

The script \texttt{get\_flanking\_sequences.rb} generates flanking sequences for
SNPs in a VCF file. Madatory parameters include the file path to the VCF file
(option -v) and the reference Fasta file (option -f). By default the length of 
the generated flanking sequences is 100bp which can be overridden by the
option -l. A maximum number of allowed SNPs within the flanking sequences
can be set with option -s (default is 0). If more than the maximum threshold
of SNPs within the flanking sequence length is detected no flanking sequence
will be generated. If the distance of the SNP to the end of the specified
sequence (e.g. a contig) is shorter than the specified flanking sequence
length than no flanking sequence will be generated. The frequency of flanking
SNPs in all samples can also be considered for flanking sequence selection
(option -{}-maximum-snps-freq). All SNPs with a frequency lower than the defined
threshold will not be considered as flanking SNPs. The flanking sequences are
stored in a new VCF file in the INFO field with the tag ``AS''. If no flanking
sequence has been generated it is indicated by a dot (i.e. ``AS=.''). SNPs that
are contained within a flanking sequence can be shown as IUPAC codes
(option -i). The output of all SNPs without a flanking sequence can be omitted
by the option -{}-filter-flanks. Custom output file names can be specified with
the option -o.

\subsubsection*{Examples}

This example takes all SNPs from the VCF file ``flank\_test.vcf'' and generates
flanking sequences based on the sequences in ``test.fasta''. Flanking sequences
of 5bp in length including a maximum of 2 neighbour SNPs will be created and
saved in a new VCF file called ``my\_new\_vcf\_file.vcf''.

\begin{lstlisting}[language=bash]
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 5 -s 2 -o my_new_vcf_file.vcf
\end{lstlisting}

Show iupac codes in flanking sequences:

\begin{lstlisting}[language=bash]
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 10 -s 2 -i -o my_new_vcf_file.vcf
\end{lstlisting}

\section{MASSArray2VCF}

The script \texttt{massarray2vcf.rb} converts a text file in MASSArray format
into a VCF file. Mandatory parameters include a MASSArray text file (option -f)
and an additional tab separated text file (option -{}-ref-file) containing the
contig id, the SNP position on the contig and the reference allele. Custom
output file names can be specified with the option -o.

\subsubsection*{Examples}

\begin{lstlisting}[language=bash]
./massarray2vcf.rb -f data/testdata/massarray.txt -r data/testdata/massarray_ref_data.txt -o my_new_vcf_file.vcf
\end{lstlisting}

\section{VCF2MASSArray}

The script \texttt{vcf2massarray.rb} converts a VCF file into a text file in
MASSArray format. The script needs a mandatory VCF file (option -f). Three
options allow to replace identifiers and allele codes by custom values.
Tha mapping of the values needs to be specified in a tab separated text file
containing two columns. The first column specifies the identifier of the input
file and the second column specifies the new identifier. The script allows to
replace locus identifiers (option -l), sample names (option -s) and allele
codes (option -a). Custom output file names can be specified with the option -o.

\subsubsection*{Examples}

\begin{lstlisting}[language=bash]
./vcf2massarray.rb -f data/testdata/pop_test.vcf -l data/testdata/loc_mapping.txt -o my_new_massarray_file.txt
\end{lstlisting}

\section{Get Read frequencies}

This script calculates allele frequencies based on read counts with respect to
different alleles in the reads. A mandatory VCF file needs to be provided
(option -f). And an output format can be chosen (option -t). One of two output
types can be selected: A VCF like format (vcf) and a MASSArray like format
(massarray). Custom output file names can be specified with the option -o.

\subsubsection*{Examples}

\begin{lstlisting}[language=bash]
./get_read_frequencies.rb -f data/testdata/pop_test2.vcf -t massarray -o pop_test2.txt
\end{lstlisting}

\section{Compare Genotypes}

The script \texttt{compare\_genotypes.rb} compares genotypes of two datasets
in VCF by calculating distance values. Input are to VCF files (options -1 and
-2) and both VCF files have to include exactly the same samples und loci.
The script can calculate two different types of distances (option -t).
The \textit{naive} option sets a fixed value for each genotype in each dataset
(0/0 $\Rightarrow$ 0; 0/1 $\Rightarrow$ 0.5; 1/1 $\Rightarrow$ 1) and then
calculates the absolute difference between the corresponding genotype values.
Finally an average distance value over all samples is calculated. The resulting
tab separated file contains the contig name, the position, the distance score
for each locus in each sample and the average distance score over all samples.
The \textit{gregorius} option calculates a distance value proposed by
Gregorius~\cite{GRE:1974,BER:1974} by counting all alleles for each locus in each dataset followed
by the sum of all absolute differences of all alleles. The resulting tab
separated file contains the contig name, the position and the calculated
distance value of all alleles at the locus over all samples. Custom output
file names can be specified with the option -o.

\subsubsection*{Examples}

\begin{lstlisting}[language=bash]
./compare_genotypes.rb -1 data/testdata/compare1.vcf -2 data/testdata/compare2.vcf -t gregorius
\end{lstlisting}

\bibliographystyle{unsrt}
\bibliography{references}

\end{document}