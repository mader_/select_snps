#!/usr/bin/env ruby

=begin
  Copyright (c) 2018-2019 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2018-2019 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require_relative 'lib/vcf'
require_relative 'lib/vcf_entry'
require_relative 'lib/plotter'
require_relative 'lib/select_snps_option_parser.rb'

require 'axlsx'

options = OptionParser.parse(ARGV)

vcfs = Array.new

#parse vcf files with corresponding options
File.open(options.file_list, "r") do |f|
  f.each_line do |line|
    if(!line.match("#"))
      sline = line.chomp.split("\t")
      vcf = VCF.new
      vcf.read_file(sline[0], "single", sline[1].to_i, sline[2].to_f)
      vcfs.push(vcf)
    end
  end
end

if (!options.group_file.eql?(""))
  #read groups file
  groups = Hash.new
  File.open(Dir.pwd + "/" + options.group_file, "r") do |f|
    f.each_line do |line|
        sline = line.chomp.split("\t")
        if(groups.has_key?(sline[0]))
          groups[sline[0]].push(sline[1])
        else
          groups.store(sline[0], [sline[1]])
        end
    end
  end
end

#merge all vcf files and sort results according to weighted qual score
mvcfs = vcfs[0].merge_vcfs(vcfs)

if(options.freq_filter != 0)
  mvcfs.filter_allele_frequency(mvcfs.vcf_entries,options.freq_filter)
end

filter = Hash.new()
if(options.filter_flank)
  filter.store("discard_flank", 1)
end
if(options.filter_qual > 0)
  filter.store("qual", options.filter_qual)
end

#filter vcf entries
mvcfs.filter_vcf(filter)
fmvcfs = mvcfs

#if population groups are defined, build genotype groups according to every group
if (!options.group_file.eql?(""))
  fmvcfs.condense_groups(groups)
end

fmvcfs.sort_and_group_vcf(options.prefix)
svcfs = fmvcfs

stats = svcfs.make_stats()

#write reults to excel sheets
p = Axlsx::Package.new
wb = p.workbook
plotter = Plotter.new

plotter.plot_stats(wb, "Statistics", stats)
plotter.plot(wb, "Sorted QUAL RAD SNPs", svcfs.vcf_header.to_ary, svcfs.groups_sorted_by_qual, false)
plotter.plot(wb, "Sorted WQUAL RAD SNPs", svcfs.vcf_header.to_ary, svcfs.groups_sorted_by_wqual, false)

if (!options.group_file.eql?(""))
  plotter.plot(wb, "Condensed by groups", svcfs.vcf_header.to_ary_c, svcfs.grouped_vcf_entries, true)
end

s = p.to_stream()
open(options.output, 'w') do |f|
  f.write(s.read)
end