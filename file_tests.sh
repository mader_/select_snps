################################################################################
#massarray2vcf tests
################################################################################

echo -n "Testing massarray2vcf...                                                    "
tput sc
./massarray2vcf.rb -f data/testdata/massarray.txt -r data/testdata/massarray_ref_data.txt
diff data/expected_results/massarray.vcf vcf_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing massarray2vcf fail...                                               "
tput sc
./massarray2vcf.rb -f data/testdata/massarray_fail.txt -r data/testdata/massarray_ref_data.txt > vcf_output.txt
diff data/expected_results/massarray_fail.txt vcf_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
rm vcf_output.vcf
rm vcf_output.txt


################################################################################
#vcf2massarray tests
################################################################################

echo -n "Testing vcf2massarray...                                                    "
tput sc
./vcf2massarray.rb -f data/testdata/pop_test.vcf
diff data/expected_results/vcf2massarray.txt massarray_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing vcf2massarray haploid...                                            "
tput sc
./vcf2massarray.rb -f data/testdata/pop_test_haploid.vcf
diff data/expected_results/vcf2massarray_haploid.txt massarray_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing vcf2massarray missing data...                                       "
tput sc
./vcf2massarray.rb -f data/testdata/pop_test_filter_missing_data.vcf
diff data/expected_results/vcf2massarray_missing_data.txt massarray_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing vcf2massarray locus synonyms...                                     "
tput sc
./vcf2massarray.rb -f data/testdata/pop_test.vcf -l data/testdata/loc_mapping.txt
diff data/expected_results/vcf2massarray_loc_synonym.txt massarray_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing vcf2massarray sample synonyms...                                    "
tput sc
./vcf2massarray.rb -f data/testdata/pop_test.vcf -s data/testdata/sample_mapping.txt
diff data/expected_results/vcf2massarray_sample_synonym.txt massarray_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing vcf2massarray allele codes...                                       "
tput sc
./vcf2massarray.rb -f data/testdata/pop_test_filter_missing_data.vcf -a example_configs/allele_codes_2.txt
diff data/expected_results/vcf2massarray_missing_data_allele_codes.txt massarray_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
rm massarray_output.txt

################################################################################
#compare genotypes tests
################################################################################

echo -n "Testing naive compare genotypes...                                          "
tput sc
./compare_genotypes.rb -1 data/testdata/compare1.vcf -2 data/testdata/compare2.vcf -t individual
diff data/expected_results/compare_gt_gregorius_individual.txt data_compare_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "... Genotype dataset 1 distribution...                                      "
tput sc
diff data/expected_results/gts_compare1.txt gts1_compare_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "... Genotype dataset 2 distribution...                                      "
tput sc
diff data/expected_results/gts_compare2.txt gts2_compare_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "... Genotype difference distribution...                                     "
tput sc
diff data/expected_results/gt_diff_compare.txt cmp_gt_compare_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
rm cmp_gt_compare_output.txt
rm gts1_compare_output.txt
rm gts2_compare_output.txt
rm data_compare_output.txt
echo -n "Testing gregorius compare genotypes...                                      "
tput sc
./compare_genotypes.rb -1 data/testdata/compare1.vcf -2 data/testdata/compare2.vcf -t population
diff data/expected_results/compare_gt_gregorius_population.txt data_compare_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "... Genotype dataset 1 distribution...                                      "
tput sc
diff data/expected_results/gts_compare1.txt gts1_compare_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "... Genotype dataset 2 distribution...                                      "
tput sc
diff data/expected_results/gts_compare2.txt gts2_compare_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "... Genotype difference distribution...                                     "
tput sc
diff data/expected_results/gt_diff_compare.txt cmp_gt_compare_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
rm cmp_gt_compare_output.txt
rm gts1_compare_output.txt
rm gts2_compare_output.txt
rm data_compare_output.txt

################################################################################
#Recall tests
################################################################################

echo -n "Testing recall...                                                           "
tput sc
./recall_variants.rb -f data/testdata/recall.vcf -t 0.7
diff data/expected_results/recall.vcf recall_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
rm recall_output.vcf


################################################################################
#VCF2GDANT tests
################################################################################

echo -n "Testing VCF2GDANT...                                                        "
tput sc
./vcf2gdant.rb -f data/testdata/genotypes_50x50.vcf -t test -g example_configs/example_groups_one_pop.txt -p SPECI -a 10
diff data/expected_results/vcf2gdant_testresults.txt gdant_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing VCF2GDANT with populations...                                       "
tput sc
./vcf2gdant.rb -f data/testdata/pop_test.vcf -t pop_test -g example_configs/example_groups.txt -p SPECI -a 10
diff data/expected_results/vcf2gdant_testresults_pop.txt gdant_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing VCF2GDANT with haploid populations...                               "
tput sc
./vcf2gdant.rb -f data/testdata/pop_test_haploid.vcf -t pop_test -g example_configs/example_groups.txt -p SPECI -a 10
diff data/expected_results/vcf2gdant_testresults_pop_haploid.txt gdant_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing VCF2GDANT with filter...                                            "
tput sc
./vcf2gdant.rb -f data/testdata/pop_test_filter.vcf -t pop_test_filter -g example_configs/example_groups.txt -c example_configs/example_group_coords.txt -p SPECI -a 10 --filter-gt-frequency 0.9
diff data/expected_results/vcf2gdant_testresults_pop_filter.txt gdant_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing VCF2GDANT with missing data...                                      "
tput sc
./vcf2gdant.rb -f data/testdata/pop_test_filter_missing_data.vcf -t pop_test_filter -g example_configs/example_groups.txt -p SPECI
diff data/expected_results/vcf2gdant_testresults_pop_missing_data.txt gdant_output.txt && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
rm gdant_output.txt

################################################################################
#Flank tests
################################################################################

#Flank 1
#-------------------------------------------------------------------------------

echo -n "Testing get_flanking_sequences with 5bp and 2 SNPs...                       "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 5 -s 2
diff data/expected_results/flank_test_5.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 10bp and 2 SNPs...                      "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 10 -s 2
diff data/expected_results/flank_test_10.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 15bp and 2 SNPs...                      "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 15 -s 2
diff data/expected_results/flank_test_15.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 20bp and 2 SNPs...                      "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 20 -s 2
diff data/expected_results/flank_test_20.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 20bp and 2 SNPs and filter...           "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 20 -s 2 --filter-flanks
diff data/expected_results/flank_test_20_filter.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init

#Flank 2
#-------------------------------------------------------------------------------

echo -n "Testing get_flanking_sequences with 4bp and 1 SNPs...                       "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2.vcf -f data/testdata/test.fasta -l 4 -s 1
diff data/expected_results/flank_test2_4.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 5bp and 1 SNPs...                       "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2.vcf -f data/testdata/test.fasta -l 5 -s 1
diff data/expected_results/flank_test2_5.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init

echo -n "Testing get_flanking_sequences with 10bp and 2 SNPs and iupac...            "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 10 -s 2 -i
diff data/expected_results/flank_test_10_iupac.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 15bp and 2 SNPs and iupac...            "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 15 -s 2 -i
diff data/expected_results/flank_test_15_iupac.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 20bp and 2 SNPs and iupac...            "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test.vcf -f data/testdata/test.fasta -l 20 -s 2 -i
diff data/expected_results/flank_test_20_iupac.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 4bp and 1 SNPs and iupac...             "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2.vcf -f data/testdata/test.fasta -l 4 -s 1 -i
diff data/expected_results/flank_test2_4_iupac.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 5bp and 1 SNPs and iupac...             "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2.vcf -f data/testdata/test.fasta -l 5 -s 1 -i
diff data/expected_results/flank_test2_5_iupac.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 4bp and 1 SNPs multi alt and iupac...   "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2_multi_alt.vcf -f data/testdata/test.fasta -l 4 -s 1 -i
diff data/expected_results/flank_test2_multi_alt_iupac.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init

echo -n "Testing get_flanking_sequences with 4bp, 1 SNP and 0.7 max frequency...     "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2_freq.vcf -f data/testdata/test.fasta -l 4 -s 1 --maximum-snps-freq 0.7
diff data/expected_results/flank_test2_4_1_freq_07.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 4bp, 1 SNP and 0.6 max frequency...     "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2_freq.vcf -f data/testdata/test.fasta -l 4 -s 1 --maximum-snps-freq 0.6
diff data/expected_results/flank_test2_4_1_freq_06.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 4bp, 0 SNP and 0.6 max frequency...     "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2_freq.vcf -f data/testdata/test.fasta -l 4 -s 0 --maximum-snps-freq 0.6
diff data/expected_results/flank_test2_4_0_freq_06.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init
echo -n "Testing get_flanking_sequences with 4bp, 0 SNP and 0.7 max frequency...     "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2_freq.vcf -f data/testdata/test.fasta -l 4 -s 0 --maximum-snps-freq 0.7
diff data/expected_results/flank_test2_4_0_freq_07.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init

echo -n "Testing get_flanking_sequences with 4bp, 0 SNP, 0.6 max frequency, iupac... "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2_freq.vcf -f data/testdata/test.fasta -l 4 -s 0 --maximum-snps-freq 0.6 -i
diff data/expected_results/flank_test2_4_0_freq_06_iupac.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init

echo -n "Testing get_flanking_sequences with 4bp, 0 SNP, 0.7 max frequency, iupac... "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2_freq.vcf -f data/testdata/test.fasta -l 4 -s 0 --maximum-snps-freq 0.7 -i
diff data/expected_results/flank_test2_4_0_freq_07_iupac.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init

echo -n "Testing get_flanking_sequences with 4bp, 1 SNP, 0.7 max frequency, iupac... "
tput sc
./get_flanking_sequences.rb -v data/testdata/flank_test2_freq.vcf -f data/testdata/test.fasta -l 4 -s 1 --maximum-snps-freq 0.7 -i
diff data/expected_results/flank_test2_4_1_freq_07_iupac.vcf flank_output.vcf && echo -e "\e[92m[OK]" || { tput rc; echo -e "\e[91m[FAIL]"; }
tput init

rm flank_output.vcf
echo "ALL DONE"