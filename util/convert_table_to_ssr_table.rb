#!/usr/bin/env ruby

=begin
  Copyright (c) 2020 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2020 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

file = ARGV[0]

similarity_thr = 0.7
max_sim_diff = 3
missing_data_val = 0

header = true

loci = Hash.new
samples = Hash.new

#read table with unknown ploidy
File.open(Dir.pwd + "/" + file, "r") do |f|
  f.each_line do |line|
    sline = line.chomp.split("\t")

    if (header)

      hline = line.split("\t")

      hline.shift
      cur_loc = nil
      hline.each do |h|
        if (h.include?("\n"))
          break
        end
        if (!h.eql?(""))
          cur_loc = h
          loci.store(h,1)
        else
          loci[cur_loc] += 1
        end
      end
      header = false
    else
      name = true
      cur_sample = nil
      locus_id = 0
      allele_id = 0
      sline.each do |s|
        if (name)
          samples.store(s, Array.new(loci.keys.size))
          cur_sample = s
          name = false
        else
          nof_locus_alleles = loci[loci.keys[locus_id]]
          if (allele_id == 0)
            samples[cur_sample][locus_id] = Array.new(nof_locus_alleles)
          end
          if (allele_id < nof_locus_alleles)
            if (s.eql?(""))
              val = 0
            else
              val = s
            end
            samples[cur_sample][locus_id][allele_id] = val
            if (nof_locus_alleles - allele_id == 1)
              locus_id += 1
              allele_id = -1
            end
          end
          allele_id += 1
        end
      end
    end
  end
end

samples_keys = samples.keys()

sample_id = Hash.new
i = 0
samples_keys.each do |k,v|
  sample_id.store(k,i)
  i += 1
end

sample_diff = Array.new(samples.size) { Array.new(samples.size, nil) }

#calculate similarity of genotype calls bewtween all samples.
samples_keys.each do |k1|
  samples_keys.each do |k2|
    if (k1.eql?(k2))
      sample_diff[sample_id[k1]][sample_id[k2]] = "-"
    else
      if (sample_diff[sample_id[k2]][sample_id[k1]] != nil)
        sample_diff[sample_id[k1]][sample_id[k2]] = "-"
        next
      end
      s1 = samples[k1]
      s2 = samples[k2]
      locus_similarity = 0
      nof_loci = 0
      for i in 0..loci.size - 1
        l1 = s1[i]
        l2 = s2[i]
        allele_similarity = 0
        nof_alleles = 0
        for j in 0..l1.size - 1
          if (l1[j].to_i != missing_data_val && l2[j].to_i != missing_data_val)
            if (l1[j].to_i == l2[j].to_i || (l1[j].to_i - l2[j].to_i).abs < max_sim_diff)
              allele_similarity += 1
            end
            nof_alleles += 1
          end
        end
        if (nof_alleles > 0)
          locus_similarity += allele_similarity.to_f / nof_alleles
          nof_loci += 1
        end
      end
      if (nof_loci > 0)
        sample_diff[sample_id[k1]][sample_id[k2]] = (locus_similarity / nof_loci).truncate(3)
      else
        sample_diff[sample_id[k1]][sample_id[k2]] = Float::NAN
      end
    end
  end
end

open("similarity_matrix.txt", 'w') do |f|
  f.puts "\t" + samples_keys.join("\t")
  sample_diff.each_with_index do |row, i|
    f.print sample_id.key(i) + "\t" + row.join("\t") + "\n"
  end
end

open("similarity_log.txt", 'w') do |f|
  index = 1
  sample_diff.each_with_index do |row, i|
    row.each_with_index do |col, j|
      if (col.is_a?(Numeric))
        if(col > similarity_thr)
          f.puts "#{index}.\tSamples \"#{sample_id.key(i)}\" and \"#{sample_id.key(j)}\" have a similarity of #{sample_diff[i][j]}."
          index += 1
        end
      end
    end
  end
end