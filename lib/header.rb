#!/usr/bin/env ruby

=begin
  Copyright (c) 2018-2019 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2018-2019 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

class Header

attr_accessor :chrom, :pos, :id, :ref, :alt, :qual, :filter, :info, :format,
              :genotypes, :condensed_genotypes, :group, :snp_id, :wqual, :meta

def initialize()
  @meta = Array.new()
end

def new_header(headerline)
  shead = headerline.split("\t")
  @chrom = shead[0]
  @pos = shead[1]
  @id = shead[2]
  @ref = shead[3]
  @alt = shead[4]
  @qual = shead[5]
  @filter = shead[6]
  @info = shead[7]
  @format = shead[8]
  @genotypes = Array.new
  for i in 9..(shead.size-1)
    @genotypes.push(shead[i])
  end
  @condensed_genotypes = Array.new
  @group = "Group"
  @snp_id = "SNP ID"
  @wqual = "Weighted Quality"
end

def set_condensed_genotypes(groups)
   groups.each do |k,v|
    @condensed_genotypes.push(k)
   end
end

def genotypes_to_s()
  str=""
  genotypes.each do |g|
    str += "\t#{g}"
  end
  return str
end

def to_s_vcf
  return "#{@chrom}\t#{@pos}\t#{@id}\t#{@ref}\t#{alt}\t#{@qual}\t#{@filter}\t" \
          "#{info}\t#{@format}#{genotypes_to_s()}"
end

def to_s_tab
  return "#{group}\t#{@chrom}\t#{@pos}\t#{@id}\t#{@ref}\t#{alt}\t#{@qual}\t" \
         "#{wqual}\t#{@filter}\t#{info}\t#{@format}#{genotypes_to_s()}"
end

def to_ary
  return [@group, @chrom, @pos, @id, @ref, @alt, @qual, @wqual, @filter, @info["AS"], @format].concat(@genotypes)
end

def to_ary_c
  return [@group, @chrom, @pos, @id, @ref, @alt, @qual, @wqual, @filter, @info["AS"], @format].concat(@condensed_genotypes)
end
end