#!/usr/bin/env ruby

=begin
  Copyright (c) 2019 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2019 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require 'optparse'
require 'ostruct'

class OptionParser
  def self.parse(args)

    options = OpenStruct.new
    options.vcf_file = ""
    options.fasta_file = ""
    options.flank_length = 100
    options.maximum_snps_allowed = 0
    options.maximum_snps_freq_allowed = 0
    options.iupac_snps = false
    options.filter_flank = false
    options.output = "flank_output.vcf"
    
    optparse = OptionParser.new do |opts|
      opts.banner = "Usage: get_flanking_sequences.rb [options] -v vcf_file -f fasta_file"

      opts.separator ""
      opts.separator "Options:"

      opts.on('-v', '--vcf-file FILE', "VCF file containing variants.") do |f|
        options.vcf_file = f
      end

      opts.on('-f', '--fasta-file FILE', "Fasta file containing the reference.",
               "sequence") do |f|
        options.fasta_file = f
      end

      opts.on('-l', '--flank-length [NUM]', Integer, "Set the desired flanking ",
              "sequence length in base pairs. (Default: 100 bp)") do |f|
        options.flank_length = f
      end

      opts.on( '-s', '--maximum-snps [NUM]', Integer, "Set the maximum number of ",
               "allowed SNPs within the flanking sequences. (Default: 0)" ) do |f|
        options.maximum_snps_allowed = f
      end

      opts.on( '--maximum-snps-freq [NUM]', Float, "Set the maximum frequency for ",
               "allowed SNPs within the flanking sequences. (Default: 0)" ) do |f|
        options.maximum_snps_freq_allowed = f
      end

      opts.on('-i', '--show-iupac-snps', "show SNPs in flanking sequences as ",
              "IUPAC codes.") do
        options.iupac_snps = true
      end

      opts.on('--filter_flanks', "remove all SNPs without flanking",
              "sequences") do
        options.filter_flank = true
      end

      opts.on('-o', '--output NAME', "Set the output filename (Default ",
               "flank_output.vcf)") do |f|
        options.output = f
      end

      opts.on('-h', '--help', 'Display this screen') do
        puts opts
        exit
      end

      opts.on_tail("--version", "Show version") do
        puts ""
        puts " Version: 0.8"
        puts ""
        puts "Copyright (c) 2018 - 2019 Malte Mader <malte.mader@thuenen.de>"
        puts "Copyright (c) 2018 - 2019 Thünen Institute of Forest Genetics"
        exit
      end
    end

    optparse.parse!(args)

    if(options.vcf_file.eql?(""))
      puts ""
      puts "A VCF file must be provided! (Option -v)"
      puts ""
      puts optparse
      exit
    end

    if(options.fasta_file.eql?(""))
      puts ""
      puts "A Fasta file containing the reference sequence must be provided! (Option -f)"
      puts ""
      puts optparse
      exit
    end

    return options
  end
end