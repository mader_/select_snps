#!/usr/bin/env ruby

=begin
  Copyright (c) 2019-2020 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2019-2020 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require 'optparse'
require 'ostruct'

class OptionParser
  def self.parse(args)

    options = OpenStruct.new
    options.vcf_file = ""
    options.title = ""
    options.group_file = ""
    options.coords_file = ""
    options.allele_codes = "example_configs/allele_codes.txt"
    options.prefix = ""
    options.highest_allel_code = 10
    options.freq_filter = 0
    options.filter_flank = false
    options.filter_qual = 0
    options.output = "gdant_output.txt"
    
    optparse = OptionParser.new do |opts|
      opts.banner = "Usage: vcf2gdant.rb [options] -f vcf_file -t title -g group_file"

      opts.separator ""
      opts.separator "Options:"

      opts.on('-f', '--vcf-file [FILE]', "VCF file containing the SNPs to be ",
               "converted") do |f|
        options.vcf_file = f
      end

      opts.on('-t', '--title [TEXT]', "Set the title for the GDANT file ") do |f|
        options.title = f
      end

      opts.on('-g', '--coordinates [FILE]', "Text file with grouping information of the ",
               "samples") do |f|
        options.group_file = f
      end

      opts.on('-c', '--coordinates [FILE]', "Text file with geographic coordinates ",
               "for every population") do |f|
        options.coords_file = f
      end

      opts.on('--allele_codes [FILE]', "Text file with definitions of allele
              codes") do |f|
        options.allele_codes = f
      end

      opts.on('-p', '--prefix [TEXT]', "Prefix for custom ID") do |f|
        options.prefix = f
      end

      opts.on( '-a', '--highest-allel-code [NUM]', Integer, "Set the highest allel count.",
               "Default 10" ) do |f|
        options.highest_allel_code = f
      end

      opts.on("--filter-gt-frequency [NUM]", Float, "Maximum frequency threshold for",
              "genotypes (0/0,1/1,2/2) to be filtered out.") do |f|
        options.freq_filter = f
      end

      opts.on('--filter_flanks', "remove all SNPs without flanking",
              "sequences") do
        options.filter_flank = true
      end

      opts.on('--quality_filter [NUM]', Integer, "Discard all variants below the",
               "provided quality value threshold" ) do |f|
        options.filter_qual = f
      end

      opts.on('-o', '--output NAME', "Set the output filename (Default ",
               "gdant_output.txt)") do |f|
        options.output = f
      end

      opts.on('-h', '--help', 'Display this screen') do
        puts opts
        exit
      end

      opts.on_tail("--version", "Show version") do
        puts ""
        puts " Version: 0.4"
        puts ""
        puts "Copyright (c) 2019-2020 Malte Mader <malte.mader@thuenen.de>"
        puts "Copyright (c) 2019-2020 Thünen Institute of Forest Genetics"
        exit
      end
    end

    optparse.parse!(args)

    if(options.vcf_file.eql?(""))
      puts ""
      puts "A VCF file containing all SNPs to be converted must be provided! (Option -f)"
      puts ""
      puts optparse
      exit
    end

    if(options.title.eql?(""))
      puts ""
      puts "A title must be provided! (Option -t)"
      puts ""
      puts optparse
      exit
    end

    if(options.group_file.eql?(""))
      puts ""
      puts "A group file specifying population groups must be provided! (Option -g)"
      puts ""
      puts optparse
      exit
    end

    return options
  end
end