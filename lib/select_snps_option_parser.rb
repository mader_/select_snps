#!/usr/bin/env ruby

=begin
  Copyright (c) 2018-2019 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2018-2019 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require 'optparse'
require 'ostruct'

class OptionParser
  def self.parse(args)

    options = OpenStruct.new
    options.file_list = ""
    options.group_file = ""
    options.prefix = ""
    options.filter_flank = false
    options.filter_qual = 0
    options.freq_filter = 0
    options.output = "snps_output.xlsx"
    
    optparse = OptionParser.new do |opts|
      opts.banner = "Usage: select_rad_snps_new.rb [options] -F filelist"

      opts.separator ""
      opts.separator "Options:"

      opts.on('-F', '--filelist FILE', "Text File containing the vcf files to be ",
               "processed") do |f|
        options.file_list = f
      end

      opts.on('-g', '--groups FILE', "Text File grouping information of the ",
               "samples") do |f|
        options.group_file = f
      end

      opts.on('-f', '--filter_flanks', "remove all SNPs without flanking",
              "sequences") do
        options.filter_flank = true
      end

      opts.on( '-q', '--quality_filter [NUM]', Integer, "Discard all variants below the",
               "provided quality value threshold" ) do |f|
        options.filter_qual = f
      end

      opts.on("--filter-gt-frequency [NUM]", Float, "Maximum frequency threshold for",
              "genotypes (0/0,1/1,2/2) to be filtered out.") do |f|
        options.freq_filter = f
      end

      opts.on('-p', '--prefix text', "Prefix for custom ID") do |f|
        options.prefix = f
      end

      opts.on('-o', '--output NAME', "Set the output filename (Default ",
               "snps_output.xlsx)") do |f|
        options.output = f
      end

      opts.on('-h', '--help', 'Display this screen') do
        puts opts
        exit
      end

      opts.on_tail("--version", "Show version") do
        puts ""
        puts " Version: 0.7"
        puts ""
        puts "Copyright (c) 2018 - 2019 Malte Mader <malte.mader@thuenen.de>"
        puts "Copyright (c) 2018 - 2019 Thünen Institute of Forest Genetics"
        exit
      end
    end

    optparse.parse!(args)

    if(options.file_list.eql?(""))
      puts ""
      puts "A text file containing all files to be processed must be provided! (Option -F)"
      puts ""
      puts optparse
      exit
    end
    return options
  end
end