#!/usr/bin/env ruby

=begin
  Copyright (c) 2020 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2020 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require 'optparse'
require 'ostruct'

class OptionParser
  def self.parse(args)
    options = OpenStruct.new
    options.vcf_file = ""
    options.het_threshold = 0.65
    options.output = "recall_output.vcf"

    optparse = OptionParser.new do |opts|
      opts.banner = "Usage: recall.rb [options] -f vcf_file"

      opts.separator ""
      opts.separator "Options:"

      opts.on('-f', '--vcf-file [FILE]', "VCF file containing the genotypes to ",
               "be readjusted.") do |f|
        options.vcf_file = f
      end

      opts.on('-t', '--frequency-threshold [NUM]', Float, "Minimum allele ",
              "frequency threshold to consider the genotype heterozygous.",
              "All genotypes above the threshold will be considered ",
              "homozygous. (Default: 0.65)") do |f|
        options.het_threshold = f
      end

      opts.on('-o', '--output NAME', "Set the output filename (Default ",
               "recall_output.vcf)") do |f|
        options.output = f
      end

      opts.on('-h', '--help', 'Display this screen') do
        puts opts
        exit
      end

      opts.on_tail("--version", "Show version") do
        puts ""
        puts " Version: 0.1"
        puts ""
        puts "Copyright (c) 2020 Malte Mader <malte.mader@thuenen.de>"
        puts "Copyright (c) 2020 Thünen Institute of Forest Genetics"
        exit
      end
    end

    optparse.parse!(args)

    if(options.vcf_file.eql?(""))
      puts ""
      puts "A VCF file containing all SNPs to be converted must be provided! (Option -f)"
      puts ""
      puts optparse
      exit
    end

    return options

  end
end