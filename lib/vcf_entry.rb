#!/usr/bin/env ruby

=begin
  Copyright (c) 2018-2021 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2018-2021 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

class VCFEntry

attr_accessor :group, :chrom, :pos, :id, :ref, :alt, :qual, :wqual,
              :filter, :info, :format, :genotypes, :gt_details,
              :condensed_genotypes, :left_neighbours, :right_neighbours,
              :store_flank, :snp_freq, :allele_dist, :num_alleles, :allele_freq

def initialize(entry)
  @chrom = entry[0]
  @pos = entry[1].to_i
  @id = entry[2]
  @ref = entry[3]
  @alt = entry[4]
  @qual = entry[5].to_i
  @wqual = entry[5].to_i
  @filter = entry[6]
  info_str = entry[7]
  @format = entry[8]
  @genotypes = Array.new
  @gt_details = Array.new
  @condensed_genotypes = Array.new

  for i in 9..(entry.size-1)
    @genotypes.push(entry[i])

    format_hash = get_format_hash()

    gt_values = entry[i].split(":")

    gt_values_hash = Hash.new

    format_hash.keys.each do |k|
      val = gt_values[format_hash[k]]
      if (val == nil)
        val = "0"
      end
      gt_values_hash.store(k, val)
    end

    ro = gt_values_hash["RO"]
    ao = gt_values_hash["AO"]

    if(ao != nil)

      split_ao = nil
  
      if(ao.include?(","))
        split_ao = ao.split(',')
      else
        split_ao = [ao]
      end

      ao_sum = 0
      ro_plus_ao = 0

      ao_ratio_split = []
      if !split_ao.empty?()
        split_ao.each do |e|
          ao_sum += e.to_i
        end

        ro_plus_ao = ro.to_f + ao_sum

        split_ao.each do |e|
          ao_ratio_split.push((e.to_i != 0 ? e.to_f / ro_plus_ao : 0).round(3))
        end
      end

      ro_ratio = (ro.to_i != 0 ? ro.to_f / ro_plus_ao : 0).round(3)

      gt_values_hash.store("RO_ratio", ro_ratio)
      gt_values_hash.store("AO_ratio_split", ao_ratio_split)
    end

    @gt_details.push(gt_values_hash)
  end

  sinfo_str = []
  if (info_str.include?(":"))
    sinfo_str = info_str.split(":")
  elsif (info_str.include?(";"))
    sinfo_str = info_str.split(";")
  else
    sinfo_str.push(info_str)
  end

  @info = Hash.new
  sinfo_str.each do |i|
    if (i.include?("="))
  	  si = i.split("=")
  	  @info.store(si[0], si[1])
    else
      @info.store(i, "no_value")
    end
  end

  gt_dist = get_allele_distribution()
  snp_freq = get_snp_frequence(gt_dist)
  @snp_freq = snp_freq
  @num_alleles = 0
end

def get_format_hash()
  format_hash = Hash.new()
  f = @format.split(":")
  f.each_with_index do |e,i|
    format_hash.store(e, i)
  end
  return format_hash
end

#This function assumes exactly the same order of genotypes for one locus and
#they have to be the same size.
def count_genotypes(genotypes1, genotypes2)
  gt1_dist = Hash.new
  gt2_dist = Hash.new
  cmp_dist = Hash.new

  if (genotypes1.size != genotypes2.size)
    raise RunTimeError, "Error: both datasets have to be the same size."
  end

  (0..genotypes1.size - 1).each do |i|
    gt1 = genotypes1[i].split(":")[0]
    gt2 = genotypes2[i].split(":")[0]

    gt1 = gt1.split("/").sort().join("/")
    gt2 = gt2.split("/").sort().join("/")

    if (gt1_dist.key?(gt1))
        gt1_dist[gt1] += 1
    else
        gt1_dist.store(gt1, 1)
    end

    if (gt2_dist.key?(gt2))
        gt2_dist[gt2] += 1
    else
        gt2_dist.store(gt2, 1)
    end

    cmp_gt = []

    if (!gt1.include?(".") && !gt2.include?("."))
      cmp_gt = [gt1,gt2].sort().join("-")

      if (cmp_dist.key?(cmp_gt))
        cmp_dist[cmp_gt] += 1
      else
        cmp_dist.store(cmp_gt, 1)
      end
    end
  end
  return [gt1_dist.sort(), gt2_dist.sort()], cmp_dist.sort()
end

def calculate_allele_distributions(genotypes, alt)
  allele_dist = Hash.new()
  num_alleles = 0
  genotypes.each do |gt|
    genotype = gt.split(":")[0].split("/")

    #refactor. Extract in function. Store somewhere central...
    alt_arr = nil
    if (alt.include?(","))
      alt_arr = alt.split(',')
    else
      alt_arr = [alt]
    end

    alleles = []
    genotype.each do |g|
      if (!g.include?("."))
        if (g.eql?("0"))
          alleles.push(@ref)
        else
          index = g.to_i - 1
          alleles.push(alt_arr[index])
        end
        num_alleles += 1
      end
    end

    alleles.each do |a|
      if (allele_dist.key?(a))
        allele_dist[a] += 1
      else
        allele_dist.store(a, 1)
      end
    end
  end
  return allele_dist, num_alleles
end

def calculate_allele_frequencies(allele_dist, num_of_alleles)
  allele_freq = Hash.new
  allele_dist.each do |k, v|
    freq = v.to_f / num_of_alleles.to_f
    allele_freq.store(k, freq)
  end
  return allele_freq
end

def calculate_genetic_distance(allele_freq, ext_allele_freq)
  sum_diff = -1

  if (!allele_freq.empty?() && !ext_allele_freq.empty?())
    sum_diff = 0

    allele_freq.each do |k, v|

      if ext_allele_freq.has_key?(k)
        freq_e = ext_allele_freq[k]
      else
        freq_e = 0
      end

      sum_diff += 0.5 * (v - freq_e).abs

      ext_allele_freq.delete(k)
    end

    ext_allele_freq.each do |k, v|
      sum_diff += 0.5 * (v - 0).abs
    end

  end

  return sum_diff
end

def compare_gts(ext_vcf_entries)
  #TODO: refactor, use one method to extract genotype information from the
  #gt string. Maybe store data in data structure after first extraction.
  cmp_values = Array.new
  gt_cmp_sum = 0
  num_of_diff = 0
  @genotypes.each_with_index do |gt,i|
    ext_gt = ext_vcf_entries.genotypes[i]
    ext_alt = ext_vcf_entries.alt

    gt1_dist, gt1_num_of_alleles = calculate_allele_distributions([gt], @alt)
    gt1_freq = calculate_allele_frequencies(gt1_dist, gt1_num_of_alleles)

    gt2_dist, gt2_num_of_alleles = calculate_allele_distributions([ext_gt], ext_alt)
    gt2_freq = calculate_allele_frequencies(gt2_dist, gt2_num_of_alleles)

    gt_cmp = calculate_genetic_distance(gt1_freq, gt2_freq)

    if (gt_cmp > -1)
      gt_cmp_sum += gt_cmp
      num_of_diff += 1
    end

    cmp_values.push(gt_cmp)
  end

  if (num_of_diff > 0)
    avg_gt_cmp = gt_cmp_sum / num_of_diff
  else
    avg_gt_cmp = -1
  end
  cmp_values.push(avg_gt_cmp)

  return cmp_values
end

#experimental method for readjusting genotype calls based on read allele
#frequencies.
def recall_variants(header, het_hom_thr)
  format_hash = get_format_hash()

  @genotypes.each_with_index do |gt, i|
    head = header.genotypes[i]
    gt_values = gt.split(":")
    gt_call = gt_values[format_hash["GT"]]
    gq = gt_values[format_hash["GQ"]]
    dp = gt_values[format_hash["DP"]]
    dpr = gt_values[format_hash["DPR"]]
    ro = gt_values[format_hash["RO"]]
    ao = gt_values[format_hash["AO"]]

    if(ao != nil)

      split_ao = nil
  
      if(ao.include?(","))
        split_ao = ao.split(',')
      else
        split_ao = [ao]
      end

      ao_sum = 0
      all_sum = 0
      if !split_ao.empty?()
        split_ao.each do |e|
          ao_sum += e.to_i
        end
        all_sum = ro.to_i + ao_sum
        ratio_split_ao = []
        split_ao.each_with_index do |e,i|
          #ratio_split_ao[i] = (e.to_i != 0 ? e.to_f / dp.to_f : 0)
          ratio_split_ao[i] = (e.to_i != 0 ? e.to_f / all_sum.to_f : 0)
        end
      end

      #ratio_ro = (ro.to_i != 0 ? ro.to_f / dp.to_f : 0)
      ratio_ro = (ro.to_i != 0 ? ro.to_f / all_sum.to_f : 0)

      ratios = Hash.new
      ratios.store(0, ratio_ro)

      ratio_split_ao.each_with_index do |e, i|
        ratios.store(i+1, e)
      end

      sorted_ratios = ratios.sort_by{|k,v|  v}.reverse().to_h

      if(gt_call.include?('/'))

        gt_call_split = gt_call.split('/')

        new_gt = []

        if gt_call_split[0].eql?(gt_call_split[1])
          if (@ref.length < 2)
            if sorted_ratios.values[0] < het_hom_thr
              new_gt = [sorted_ratios.keys[0],sorted_ratios.keys[1]].sort()
              #write ne gt to current vcfentry

              new_gt_str = new_gt[0].to_s + "/" + new_gt[1].to_s

              format_hash.each do |k,v|
                if !k.eql?("GT")
                  new_gt_str += ":" + gt_values[v].to_s
                end
              end

              #puts()
              #puts(new_gt_str)

              #puts()
              #puts(new_gt)
              #puts()

              #puts()
              #puts("Scaffold: " + @chrom)
              #puts("Pos: " + @pos.to_s)
              #puts(head)
              #puts(gt_call_split)
              #puts("REF: #{@ref}")
              #puts("ALT: #{@alt}")
              #puts("Call: " + gt_call)
              #puts("GQ: " + gq)
              #puts("DP: #{dp}")
              #puts("DPR: #{dpr}")
              #puts("RO: #{ro}")
              #puts("AO: #{ao}")
              #puts("RO ratio: #{ratio_ro}")
              #ratio_split_ao.each_with_index do |e,i|
              #  puts((i+1).to_s + ". AO ratio: " + e.to_s)
              #end

              @genotypes[i] = new_gt_str
            end
          end
        else
          if (@ref.length < 2)
            if sorted_ratios.values[0] > het_hom_thr
              new_gt = [sorted_ratios.keys[0],sorted_ratios.keys[0]].sort()
              #write ne gt to current vcfentry

              new_gt_str = new_gt[0].to_s + "/" + new_gt[1].to_s

              format_hash.each do |k,v|
                if !k.eql?("GT")
                  new_gt_str += ":" + gt_values[v].to_s
                end
              end

              #puts()
              #puts(new_gt_str)

              #puts()
              #puts(new_gt)
              #puts()
  
              #puts()
              #puts("Scaffold: " + @chrom)
              #puts("Pos: " + @pos.to_s)
              #puts(head)
              #puts(gt_call_split)
              #puts("REF: #{@ref}")
              #puts("ALT: #{@alt}")
              #puts("Call: " + gt_call)
              #puts("GQ: " + gq)
              #puts("DP: #{dp}")
              #puts("DPR: #{dpr}")
              #puts("RO: #{ro}")
              #puts("AO: #{ao}")
              #puts("RO ratio: #{ratio_ro}")
              #ratio_split_ao.each_with_index do |e,i|
                #puts((i+1).to_s + ". AO ratio: " + e.to_s)
              #end

              @genotypes[i] = new_gt_str
            end
          end
        end
      end
    end
  end
end

#Get distributions of homozygous and heterozygous calls and the allele
#freqeuncies.
def get_read_cov_stats(header, stats)
  format_hash = Hash.new()
  f = @format.split(":")
  f.each_with_index do |e,i|
    format_hash.store(e, i)
  end
  @genotypes.each_with_index do |gt, i|
    gt_values = gt.split(":")
    gt_call = gt_values[format_hash["GT"]]
    gq = gt_values[format_hash["GQ"]]
    dp = gt_values[format_hash["DP"]]
    dpr = gt_values[format_hash["DPR"]]
    ro = gt_values[format_hash["RO"]]
    ao = gt_values[format_hash["AO"]]

    if(ao != nil)

      split_ao = nil
  
      if(ao.include?(","))
        split_ao = ao.split(',')
      else
        split_ao = [ao]
      end

      ao_sum = 0

      if !split_ao.empty?()
        ratio_split_ao = []
        split_ao.each_with_index do |e,i|
          ao_sum += e.to_i
          ratio_split_ao[i] = (e.to_i != 0 ? e.to_f / dp.to_f : 0)
        end
      end
      ro_plus_ao = ro.to_i + ao_sum.to_i

      ratio_ro = (ro.to_i != 0 ? ro.to_f / dp.to_f : 0)
      ratio_ao = (ao_sum.to_i != 0 ? ao_sum.to_f / dp.to_f : 0)

      head = header.genotypes[i]
      if(gt_call.include?('/'))

        gt_zero = 0
        if(ratio_split_ao.size > 1)
          ratio_split_ao.each do |e|
            gt_z = e > 0
            gt_zero += gt_z ? 1 : 0
          end
        end

        gt_call_split = gt_call.split('/')

        if gt_call_split[0].eql?(gt_call_split[1])

          if(ratio_split_ao.size > 1)
            if(gt_zero > 1)
              ratio_split_ao.each do |e|
                stats.hom_ao_split_ratio.push(e)
              end
            end
          end

          stats.hom_ro_ratio.push(ratio_ro)
          stats.hom_ao_ratio.push(ratio_ao)

          #if (false)
          #if(@chrom.eql?("RAD_kmer_0027406_1_NODE_1_length_140_cov_4064286") && ratio_ro > 0.4 && ratio_ro < 0.6) 
          #if(ratio_ro > 0.1 && ratio_ro < 0.9)
          #if(ratio_ro < 0.09)
          #if(ratio_split_ao.size > 1 && gt_zero > 1)
          #  puts()
          #  puts("Scaffold: " + @chrom)
          #  puts("Pos: " + @pos.to_s)
          #  puts(head)
          #  puts(gt_call_split)
          #  puts("REF: #{@ref}")
          #  puts("ALT: #{@alt}")
          #  puts("Call: " + gt_call)
          #  puts("GQ: " + gq)
          #  puts("DP: #{dp}")
          #  puts("DPR: #{dpr}")
          #  puts("RO: #{ro}")
          #  puts("AO: #{ao}")
          #  puts("RO + AO: #{ro_plus_ao}")
          #  puts("RO ratio: #{ratio_ro}")
          #  puts("AO ratio: #{ratio_ao}")
          #  ratio_split_ao.each_with_index do |e,i|
          #    puts((i+1).to_s + ". AO ratio: " + e.to_s)
          #  end
          #  puts()
          #end

        else

          if(ratio_split_ao.size > 1)
            if(gt_zero > 1)
              ratio_split_ao.each do |e|
                stats.het_ao_split_ratio.push(e)
              end
            end
          end

          stats.het_ro_ratio.push(ratio_ro)
          stats.het_ao_ratio.push(ratio_ao)
          
        end
      end
    end
  end
end

def get_allele_distribution()
  gt_dist = Hash.new()
  @genotypes.each do |gt|
    alleles = gt.split(":")[0]
    if(gt_dist.key?(alleles))
      gt_dist[alleles] += 1
    else
      gt_dist.store(alleles, 1)
    end
  end
  return gt_dist
end

def get_max_allele_frequency(gt_dist)
  max_freq = 0
  gt_dist.each do |k,v|
    freq = v.to_f / @genotypes.size.to_f
    if (freq > max_freq && (k.eql?("1/1") || k.eql?("0/0") || k.eql?("2/2")))
      max_freq = freq
    end
  end
  return max_freq
end

def get_snp_frequence(gt_dist)
  freq = 0
  no_snp = gt_dist["0/0"]
  freq = (@genotypes.size.to_f - no_snp.to_f) / @genotypes.size.to_f
  return freq
end

def condense_gt(groups, header)
  groups.each do |k,v|
    allel1 = 0
    allel2 = 0
    allel3 = 0
    v.each do |sample|
      index = header.genotypes.index(sample)
      gt = genotypes[index].split(":")[0].split("/")
      gt.each do |value|
        if(!value.eql?("."))
          v = value.to_i
          if (v == 0)
            allel1 += 1
          end
          if (v == 1)
            allel2 += 1
          end
          if (v == 2)
            allel3 += 1
          end
        end
      end
    end
    allel_ratio = allel1.to_s + ":" + allel2.to_s
    if(allel3 > 0)
      allel_ratio += ":" + allel3.to_s
    end
    condensed_genotypes.push(allel_ratio)
  end
end

def genotypes_to_s()
  str=""
  genotypes.each do |g|
    str += "\t#{g}"
  end
  return str
end

def read_freq_to_s()
  str = ""
  gt_details.each_with_index do |v, i|
    ro_ratio = v["RO_ratio"].to_s
    ao_ratio = ""
    v["AO_ratio_split"].each_with_index do |ao, j|
      ao_ratio += ao.to_s
      if (j < v["AO_ratio_split"].length - 1)
        ao_ratio += ","
      end
    end
    read_ratios = ro_ratio + "/" + ao_ratio
    str += read_ratios
    if (i < gt_details.length - 1)
      str += "\t"
    end
  end
  return str
end

def info_to_s()
  str = ""
  @info.each_with_index do |i, index|
    if (i[1].eql?("no_value"))
      str += i[0]
    else
      str += "#{i[0]}=#{i[1]}"
    end
    if(index < @info.length - 1)
      str += ";"
    end
  end
  return str
end

def to_s_vcf
  return "#{@chrom}\t#{@pos}\t#{@id}\t#{@ref}\t#{alt}\t#{@qual}\t#{@filter}\t" \
          "#{info_to_s()}\t#{@format}#{genotypes_to_s()}"
end

def to_s_tab
  return "#{@group}\t#{@chrom}\t#{@pos}\t#{@id}\t#{@ref}\t#{alt}\t#{@qual}\t" \
         "#{wqual}\t#{@filter}\t#{info["AS"]}\t#{@format}#{genotypes_to_s()}"
end

def to_s_read_freq
  return "#{@chrom}\t#{@pos}\t#{@id}\t#{@ref}\t#{alt}\t#{read_freq_to_s()}"
end

def to_ary
  return [@group, @chrom, @pos, @id, @ref, @alt, @qual, @wqual, @filter, @info["AS"], @format].concat(@genotypes)
end

def to_ary_c
  return [@group, @chrom, @pos, @id, @ref, @alt, @qual, @wqual, @filter, @info["AS"], @format].concat(@condensed_genotypes)
end

end