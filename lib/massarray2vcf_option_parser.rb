#!/usr/bin/env ruby

=begin
  Copyright (c) 2020 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2020 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require 'optparse'
require 'ostruct'

class OptionParser
  def self.parse(args)

    options = OpenStruct.new
    options.massarray_file = ""
    options.ref_data_file = ""
    options.output = "vcf_output.vcf"

    optparse = OptionParser.new do |opts|
      opts.banner = "Usage: massarray2vcf.rb [options] -f massarray_file -r ref_data_file"

      opts.separator ""
      opts.separator "Options:"

      opts.on('-f', '--massarray-file [FILE]', "MASSArray file for conversion") do |f|
        options.massarray_file = f
      end

      opts.on('-r', '--ref-file [FILE]', "Additional ref data") do |f|
        options.ref_data_file = f
      end

      opts.on('-o', '--output NAME', "Set the output filename (Default ",
               "vcf_output.txt)") do |f|
        options.output = f
      end

      opts.on('-h', '--help', 'Display this screen') do
        puts opts
        exit
      end

      opts.on_tail("--version", "Show version") do
        puts ""
        puts " Version: 0.1"
        puts ""
        puts "Copyright (c) 2020 Malte Mader <malte.mader@thuenen.de>"
        puts "Copyright (c) 2020 Thünen Institute of Forest Genetics"
        exit
      end
    end

    optparse.parse!(args)

    if(options.massarray_file.eql?(""))
      puts ""
      puts "A MASSArray file must be provided."
      puts ""
      puts optparse
      exit
    end

    if(options.ref_data_file.eql?(""))
      puts ""
      puts "A ref data file must be provided."
      puts ""
      puts optparse
      exit
    end

    return options
  end
end