#!/usr/bin/env ruby

=begin
  Copyright (c) 2018-2020 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2018-2020 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require_relative 'header'
require_relative 'vcf_entry'
require_relative 'stats'
require_relative 'gt_cmp'

require 'axlsx'
require 'bio'
require 'byebug'
require 'histogram/array'

class VCF

attr_accessor :vcf_header, :vcf_entries, :grouped_vcf_entries,
              :groups_sorted_by_qual, :groups_sorted_by_wqual,
              :ref_seqs, :flanking_sequence_length, :multiplier,
              :condensed_groups

def initialize()
  @vcf_entries = Hash.new
  @grouped_vcf_entries = Hash.new
  @groups_sorted_by_qual = Hash.new
  @groups_sorted_by_wqual = Hash.new
  @condensed_groups = Hash.new
end

def condense_groups(groups)
  @vcf_header.set_condensed_genotypes(groups)
  @vcf_entries.each do |k, vcf_entry|
    vcf_entry.condense_gt(groups, vcf_header)
  end
end

def filter_allele_frequency(vcf_entries,max_freq_thr)
  vcf_entries.each do |k, entry|
    dist = entry.get_allele_distribution()
    max_freq = entry.get_max_allele_frequency(dist)
    if(max_freq > max_freq_thr)
      vcf_entries.delete(k)
    end
  end
end

def filter_vcf(filter)
  @vcf_entries.each do |k,v|
    #filter for flanking seq
    if(filter["discard_flank"] && v.info["AS"].eql?("."))
      @vcf_entries.delete(k)
    end
    #filter for qual
    if(filter.key?("qual"))
      if(v.qual < filter["qual"])
        @vcf_entries.delete(k)
      end
    end
  end
end

#read vcf file and store it in a simple vcf data structure
#mode determines how the internal ID is build. "single" for single vcf file use
#and "compare" for comparison of two VCF files.
def read_file(filename, mode, flank_length, multiplier)
  File.open(filename, "r") do |f|
    @vcf_header = Header.new()
    f.each_line do |line|
      sline = line.chomp.split("\t")
      if(line.match("##"))
        @vcf_header.meta.push(sline.join("\t"))
      end
      if(line.match("CHROM"))
        eline = line.chomp
        @vcf_header.new_header(eline)
        @flanking_sequence_length = flank_length
        @multiplier = multiplier
      end
      if(!line.match("#"))
        vcf_entry = VCFEntry.new(sline)
        key = nil
        if (mode.eql?("single"))
          key = vcf_entry.chrom + "_" + vcf_entry.pos.to_s + "_" + vcf_entry.alt
        end
        if (mode.eql?("compare"))
          key = vcf_entry.chrom + "_" + vcf_entry.pos.to_s
        end
        @vcf_entries.store(key, vcf_entry)
      end
    end
  end
end

def read_massarray(file_mass, file_alleles)

  alleles = Hash.new
  File.open(file_alleles, "r") do |f|
    f.each_line do |line|
      sline = line.chomp.split("\t")
      alleles.store(sline[0], sline)
    end
  end

  header = true

  File.open(file_mass, "r") do |f|
    @vcf_header = Header.new()
    @vcf_header.chrom = "#CHROM"
    @vcf_header.pos = "POS"
    @vcf_header.id = "ID"
    @vcf_header.ref = "REF"
    @vcf_header.alt = "ALT"
    @vcf_header.qual = "QUAL"
    @vcf_header.filter = "FILTER"
    @vcf_header.info = "INFO"
    @vcf_header.format = "FORMAT"
    @vcf_header.genotypes = Array.new

    f.each_line do |line|
      sline = line.chomp.split("\t")
      if(header)
        sline.shift()

        sline = sline.uniq
        sline.each do |e|
          pos = alleles[e][1]
          ref = alleles[e][2]
          e_short = e.split("_")
          e_short.delete_at(-1)
          entry = [e_short.join("_"),pos,".",ref,".",".",".",".","."]
          vcf_entry = VCFEntry.new(entry)
          @vcf_entries.store(e, vcf_entry)
        end
        header = false
      else
        @vcf_header.genotypes.push(sline[0])
        sline.shift()

        gt_count = 1
        cur_gt = nil
        index = 0
        sline.each do |gt|
          if (gt_count < 2)
            cur_gt = [gt]
            gt_count += 1
          else
            cur_gt.push(gt)
            cur_gt_code = [0,0]

            ref = @vcf_entries[@vcf_entries.keys[index]].ref
            alt = @vcf_entries[@vcf_entries.keys[index]].alt

            alt_arr = []
            if (alt.include?(","))
              alt_arr = alt.split(",")
            else
              if (!alt.include?("."))
                alt_arr = [alt]
              end
            end

            cur_gt.each_with_index do |gtc, i|

              if (!gtc.eql?(ref))
                if (gtc.eql?("") || gtc.eql?("N"))
                  cur_gt_code[i] = "."
                else
                  if (!alt_arr.include?(gtc))
                    alt_arr.push(gtc)
                  end
                  cur_gt_code[i] = alt_arr.find_index(gtc) + 1
                end
              end
            end

            if alt_arr.size > 1
              alt = alt_arr.join(",")
            elsif alt_arr.size == 0
              alt = "."
            else
              alt = alt_arr[0]
            end

            @vcf_entries[@vcf_entries.keys[index]].alt = alt

            if ((cur_gt_code[0].to_s.eql?(".") && !cur_gt_code[1].to_s.eql?(".")) \
              || (!cur_gt_code[0].to_s.eql?(".") && cur_gt_code[1].to_s.eql?(".")))
              raise TypeError, "Error: Alleles can only all or none be undefined. " +
                    "(#{@vcf_entries[@vcf_entries.keys[index]].chrom} " +
                    "#{@vcf_entries[@vcf_entries.keys[index]].pos} " +
                    "#{cur_gt[0]} #{cur_gt[1]})"
            end

            cur_gt_code_sorted = cur_gt_code.sort()

            store_gt = cur_gt_code_sorted.join("/")

            @vcf_entries[@vcf_entries.keys[index]].genotypes.push(store_gt)
            @vcf_entries[@vcf_entries.keys[index]].format = "GT"
            gt_count = 1
            index += 1
          end
        end
      end
    end
  end
end

def cmp_gt(vcf, type)
  cmp_data = Hash.new
  all_gt_dists = Hash.new
  all_cmp_dists = Hash.new
  sorted_head1 = @vcf_header.genotypes.sort().join("\t")
  sorted_head2 = vcf.vcf_header.genotypes.sort().join("\t")
  head1 = @vcf_header.to_s_vcf
  head2 = vcf.vcf_header.to_s_vcf
  if(!sorted_head1.eql?(sorted_head2))
    raise RuntimeError, "Error: The datasets need to include exactly the same samples!"
  end
  @vcf_entries.each do |k,entry1|
    entry2 = vcf.vcf_entries[k]
    if (entry2 == nil)
      raise RuntimeError, "Error: One dataset is missing a locus: " +
            "#{entry1.chrom} #{entry1.pos}"
    end

    cmp_entries = nil
    if (type.eql?(:individual))
      cmp_entries = entry1.compare_gts(entry2)
    elsif(type.eql?(:population))

      entry1.allele_dist, entry1.num_alleles = entry1.calculate_allele_distributions(entry1.genotypes, entry1.alt)
      entry1.allele_freq = entry1.calculate_allele_frequencies(entry1.allele_dist, entry1.num_alleles)

      entry2.allele_dist, entry2.num_alleles = entry2.calculate_allele_distributions(entry2.genotypes, entry2.alt)
      entry2.allele_freq = entry2.calculate_allele_frequencies(entry2.allele_dist, entry2.num_alleles)

      cmp_entries = entry1.calculate_genetic_distance(entry1.allele_freq, entry2.allele_freq.clone())
    end

    gt_dists, cmp_dist = entry1.count_genotypes(entry1.genotypes, entry2.genotypes)
    all_gt_dists.store(k, gt_dists)
    if(!cmp_dist.empty?())
      all_cmp_dists.store(k, cmp_dist)
    end

    split_id = k.split("_")
    pos = split_id[-1]
    if(split_id.size > 2)
      split_id.pop()
      id = split_id.join("_")
    else
      id = split_id[0]
    end

    snp_info = [id, pos]
    data = snp_info.concat([cmp_entries])
    cmp_data.store(k, data)
  end

  header = nil
  header_col = nil
  if (type.eql?(:individual))
    header_col = "Avg distance"
    header = @vcf_header.genotypes[0 .. @vcf_header.genotypes.size]
  elsif(type.eql?(:population))
    header_col = "Distance"
    header = []
  end

  header.push(header_col)
  header_snp_info = ["Locus", "Pos"]
  full_header = header_snp_info.concat(header)

  gt_cmp = GenotypeComparison.new(full_header, cmp_data)
  gt_cmp.gt_dists = all_gt_dists
  gt_cmp.cmp_dists = all_cmp_dists

  return gt_cmp
end

def recall_variants(het_threshold)
  @vcf_entries.each do |k, vcf_entry|
    vcf_entry.recall_variants(@vcf_header, het_threshold)
  end
end

def calculate_read_coverage_stats()
  stats = Stats.new

  @vcf_entries.each do |k, vcf_entry|
    vcf_entry.get_read_cov_stats(@vcf_header, stats)
  end

  (bins, freqs) = stats.hom_ro_ratio.histogram

  open("hom_ro_ratio.txt", 'w') do |f|
    bins.each_with_index do |e,i|
      f.puts(e.to_s + " " + freqs[i].to_s)
    end
  end

  (bins, freqs) = stats.hom_ao_ratio.histogram

  open("hom_ao_ratio.txt", 'w') do |f|
    bins.each_with_index do |e,i|
      f.puts(e.to_s + " " + freqs[i].to_s)
    end
  end

   (bins, freqs) = stats.het_ro_ratio.histogram

  open("het_ro_ratio.txt", 'w') do |f|
    bins.each_with_index do |e,i|
      f.puts(e.to_s + " " + freqs[i].to_s)
    end
  end

  (bins, freqs) = stats.het_ao_ratio.histogram

  open("het_ao_ratio.txt", 'w') do |f|
    bins.each_with_index do |e,i|
      f.puts(e.to_s + " " + freqs[i].to_s)
    end
  end

  (bins, freqs) = stats.het_ao_split_ratio.histogram

  open("het_ao_split_ratio.txt", 'w') do |f|
    bins.each_with_index do |e,i|
      f.puts(e.to_s + " " + freqs[i].to_s)
    end
  end

  (bins, freqs) = stats.hom_ao_split_ratio.histogram

  open("hom_ao_split_ratio.txt", 'w') do |f|
    bins.each_with_index do |e,i|
      f.puts(e.to_s + " " + freqs[i].to_s)
    end
  end

end

def merge_vcfs(vcfs)
  #sort array
  vcfs_sorted = vcfs.sort { |a, b| a.flanking_sequence_length <=> b.flanking_sequence_length}

  merged_vcf = VCF.new()
  merged_vcf.vcf_header = vcfs_sorted[0].vcf_header
  merged_vcf.flanking_sequence_length = 0
  merged_vcf.multiplier = 1

  max_flank_length = 0

  vcfs_sorted.each do |vcf|
    vcf.vcf_entries.each do |k,vcf_entry|
      if(merged_vcf.vcf_entries.key?(k))
        if(!vcf_entry.info["AS"].eql?(".") && vcf.flanking_sequence_length > max_flank_length)
          #update flanking sequence
          merged_vcf.vcf_entries[k].info["AS"] = vcf_entry.info["AS"]
          #update wqual based on flanking sequence
          merged_vcf.vcf_entries[k].wqual = merged_vcf.vcf_entries[k].qual * vcf.multiplier
        end
      else
        #add new entry
        merged_vcf.vcf_entries.store(k, vcf_entry)
        #set wqual based on flanking seq
        merged_vcf.vcf_entries[k].wqual = merged_vcf.vcf_entries[k].qual * vcf.multiplier
      end
    end
    if(vcf.flanking_sequence_length > max_flank_length)
      max_flank_length = vcf.flanking_sequence_length
    end
  end
  return merged_vcf
end

def sort_and_group_vcf(prefix)
  #build genotype groups
  @vcf_entries.each do |k,vcf_entry|
    group_string = ""
    #determine genotype group string
    vcf_entry.genotypes.each_with_index do |s,i|
    format = s.split(":")

      if(i < vcf_entry.genotypes.size - 1) 
        group_string << format[0] + ":"
      else
        group_string << format[0]
      end
    end

    vcf_entry.group = group_string

    suffix = vcf_entry.chrom + "_" + vcf_entry.pos.to_s

    if(!prefix.eql?(""))
      vcf_entry.id = prefix + "_" + suffix
    else 
      vcf_entry.id = suffix
    end

    #group all vcf entries by group string
    if(!@grouped_vcf_entries.key?(group_string))
      arr = Array.new
      arr.push(vcf_entry)
      @grouped_vcf_entries.store(group_string, arr)
    else
      @grouped_vcf_entries[group_string].push(vcf_entry)
    end
  end

    @grouped_vcf_entries = Hash[ @grouped_vcf_entries.sort_by { |k, v|  v.size}.reverse ]

    #sort groups by quality value and weighted quality value
    @grouped_vcf_entries.each do |k,vcf_entry|
      vcf_entry_sorted = vcf_entry.sort { |a, b| b.qual <=> a.qual}
      @groups_sorted_by_qual[k] = vcf_entry_sorted
      vcf_entry_sorted = vcf_entry.sort { |a, b| b.wqual <=> a.wqual}
      @groups_sorted_by_wqual[k] = vcf_entry_sorted
    end
end

def make_stats()
  group_sizes = Hash.new
  @groups_sorted_by_qual.each do |k, v|
    group_sizes.store(k, v.size)
  end
  return group_sizes
end

def read_fasta(file)
  @ref_seqs = Hash.new
  fasta = Bio::FlatFile.open(Bio::FastaFormat, file)
  fasta.each do |ff|
    @ref_seqs.store(ff.definition,ff)
  end
end

def set_flanking_seq(entry, iupac, max_freq)
  #get flanking sequences for the current SNPs and store them in the
  #INFO field with the tag "AS". If no flanking sequence should be stored
  #set "AS=.".
  if(entry.store_flank)
    left_flank = @ref_seqs[entry.chrom].seq[entry.pos - 1 - @flanking_sequence_length .. entry.pos - 2] #was ist falls < 0?
    right_flank = @ref_seqs[entry.chrom].seq[entry.pos .. entry.pos + @flanking_sequence_length - 1] #was ist falls > Ende?
    if(iupac)
      entry.left_neighbours.each do |n|
        if(n.snp_freq > max_freq)
          distance = entry.pos - n.pos
          alts = n.alt.split(",")
          seqs = [ Bio::Sequence::NA.new(n.ref) ]
          alts.each do |a|
            seqs.push(Bio::Sequence::NA.new(a))
          end
          alignment = Bio::Alignment.new(seqs)
          left_flank[-1 * distance] = alignment.consensus_iupac.upcase
        end
      end
      entry.right_neighbours.each do |n|
        if(n.snp_freq > max_freq)
          distance = n.pos - entry.pos
          alts = n.alt.split(",")
          seqs = [ Bio::Sequence::NA.new(n.ref) ]
          alts.each do |a|
            seqs.push(Bio::Sequence::NA.new(a))
          end
          alignment = Bio::Alignment.new(seqs)
          right_flank[distance - 1] = alignment.consensus_iupac.upcase
        end
      end
    end
    flanking_seqs = left_flank + "[" +  entry.ref + "/" + entry.alt + "]" + right_flank
    if(entry.info.has_key?("AS"))
      entry.info["AS"] = flanking_seqs
    else
      entry.info.store("AS", flanking_seqs)
    end
  else
    if(entry.info.has_key?("AS"))
      entry.info["AS"] = "."
    else
      entry.info.store("AS", ".")
    end
  end
end

def set_flanking_snps(index, left_snp_count, right_snp_count, contig_entries, entry)
  #Set neighbouring SNPs
      entry.store_flank = true
      (1 .. right_snp_count).each do |i|
        if(index + i < contig_entries.length)
          if (contig_entries[index + i].pos - entry.pos <= @flanking_sequence_length)
            entry.right_neighbours.push(contig_entries[index + i])
          end
        end
      end
      (1 .. left_snp_count).each do |i|
        if(index - i >= 0)
          if (entry.pos - contig_entries[index - i].pos <= @flanking_sequence_length)
            entry.left_neighbours.push(contig_entries[index - i])
          end
        end
      end
end 

def calculate_flanking_sequence(number_of_snps, max_snp_freq, iupac_snps)
  entries = @vcf_entries.values
  left_snp_count = number_of_snps
  right_snp_count = number_of_snps
  #split SNPs in arrays corresponding to their contigs
  structured_entries = Array.new()
  current_contig = nil
  next_contig = nil
  entries.each_with_index do |v, index|
    if(index > 0)
      if(v.chrom.eql?(entries[index - 1].chrom))
        current_contig.push(v)
      else
        next_contig = Array.new()
        next_contig.push(v)
        current_contig = next_contig
        structured_entries.push(current_contig)
      end
    else
      current_contig = Array.new()
      current_contig.push(v)
      structured_entries.push(current_contig)
    end
  end

  #Check for all SNPs if they are eligible for flanking sequences. If yes,
  #calculate flanking sequences.
  structured_entries.each do |contig_entries|
    contig_entries.each_with_index do |entry, index|
      #first get all neighbouring SNPs for each SNP within a defined range of
      #base pairs.
      entry.left_neighbours = Array.new
      entry.right_neighbours = Array.new
      entry.store_flank = false
      left_snp_count = number_of_snps
      right_snp_count = number_of_snps

      #if the SNP is too close to one of the contigs ends, then don't save any
      #flanking sequences.
      if(entry.pos <= @flanking_sequence_length ||
        @ref_seqs[entry.chrom].length - entry.pos < @flanking_sequence_length)
        set_flanking_seq(entry, iupac_snps, max_snp_freq)
        next
      end
    
      #if more than the maximum number of allowd SNPs (on the same contig) are
      #located within the given genomic range, then don't get neighbouring SNPs
      #as no flanking sequences will be set.
      if(index - (number_of_snps + 1) >= 0)
        min_index = index - (number_of_snps + 1)
        if(entry.pos - contig_entries[min_index].pos <= @flanking_sequence_length)
          if(max_snp_freq > 0)
            left_index = index - 1
            max_snp_count = 0
            while(entry.pos - contig_entries[left_index].pos <= @flanking_sequence_length)
              left_entry = contig_entries[left_index]
              left_snp_count += 1
              if(left_entry.snp_freq > max_snp_freq)
                max_snp_count += 1
              end
              left_index -= 1
              if(left_index < 0)
                break
              end
            end
            if(max_snp_count > number_of_snps)
              set_flanking_seq(entry, iupac_snps, max_snp_freq)
              next
            end
          else
            set_flanking_seq(entry, iupac_snps, max_snp_freq)
            next
          end
        end
      end
      if(index + number_of_snps + 1 < contig_entries.length)
        max_index = index + number_of_snps + 1
        if(contig_entries[max_index].pos - entry.pos <= @flanking_sequence_length)
          if(max_snp_freq > 0)
            right_index = index + 1
            max_snp_count = 0
            while(contig_entries[right_index].pos - entry.pos <= @flanking_sequence_length)
              right_entry = contig_entries[right_index]
              right_snp_count += 1
              if(right_entry.snp_freq > max_snp_freq)
                max_snp_count += 1
              end
              right_index += 1
              if(right_index > contig_entries.length - 1)
                break
              end
            end
            if(max_snp_count > number_of_snps)
              set_flanking_seq(entry, iupac_snps, max_snp_freq)
              next
            end
          else
            set_flanking_seq(entry, iupac_snps, max_snp_freq)
            next
          end
        end
      end

      #set_flanking_snps(index, number_of_snps, contig_entries, entry)
      set_flanking_snps(index, left_snp_count, right_snp_count, contig_entries, entry)

      #The actual flanking sequences will be set here
      set_flanking_seq(entry, iupac_snps, max_snp_freq)
      
    end
  end
end

end

