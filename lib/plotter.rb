#!/usr/bin/env ruby

=begin
  Copyright (c) 2018-2021 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2018-2021 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require_relative 'header'
require_relative 'vcf'
require_relative 'vcf_entry'


require 'axlsx'
require 'byebug'

class Plotter

def initialize()
end

#write number of groups and group sizes to sheet,
def plot_stats(workbook, title, stats)
  workbook.styles do |s|
    workbook.add_worksheet(:name => title) do |sheet|
      sheet.add_row(["Group ID", "Number of SNPs"])
      stats.each do |k,v|
        sheet.add_row([k, v])
      end
      sheet.add_row(["Number of Groups:", stats.size])
    end
  end
end

#write data to excel sheet
def plot(workbook, title, header, vcf_data, condensed)

  gt_color = Hash.new

  gt_color.store("0/0","7FC97F")
  gt_color.store("0/1","BEAED4")
  gt_color.store("1/1","FDC086")
  gt_color.store("1/2","FFFF99")
  gt_color.store("2/2","386CB0")

  workbook.styles do |s|
    workbook.add_worksheet(:name => title) do |sheet|
      sheet.add_row(header)
      vcf_data.each do |k,v|
        color = "%06x" % (rand * 0xffffff)
        cell_style = s.add_style(:bg_color => color)
        #select_style = s.add_style(:bg_color => "00FF00")
        v.each do |vcf_entry|
          arr = nil
          gt = nil
          if(condensed)
            arr = vcf_entry.to_ary_c
            gt = vcf_entry.condensed_genotypes
          else
            arr = vcf_entry.to_ary
            gt = vcf_entry.genotypes
          end
          sheet.add_row(arr, :style => [cell_style])
          i = 0
          while(i < gt.size) do
            style = s.add_style(:bg_color => gt_color[gt[i].split(":")[0]])
            sheet.rows.last.cells[i + 11].style = style
            i += 1
          end
        end
      end
      sheet.column_widths(10,10,10,10,3,4,6,6,6,25,8)
    end
  end
end

def write_vcf(vcf, filename)
  open(filename, 'w') do |f|
    vcf.vcf_header.meta.each do |m|
      f.puts m
    end
    f.puts vcf.vcf_header.to_s_vcf
    vcf.vcf_entries.each do |k,v|
      f.puts v.to_s_vcf
    end
  end
end

def write_vcf_read_frequencies(vcf, file_format, filename)
  if (file_format == :vcf)
    open(filename, 'w') do |f|
      f.puts "#CHROM\tPOS\tID\tREF\tALT" + vcf.vcf_header.genotypes_to_s
      vcf.vcf_entries.each do |k,v|
        f.puts v.to_s_read_freq
      end
    end
  end
  if(file_format == :massarray)
    open(filename, 'w') do |f|
      massarray_header = "\t"
      massarray_data = Hash.new
      vcf.vcf_entries.values.each_with_index do |v,i|
        massarray_header += v.chrom + "_" + v.pos.to_s + " (REF)\t" + v.chrom + "_" + v.pos.to_s + " (ALT)"
        if (i < vcf.vcf_entries.values.size() - 1)
          massarray_header += "\t"
        end

        v.gt_details.each_with_index do |d,j|
          if (!massarray_data.has_key?(vcf.vcf_header.genotypes[j]))
            massarray_data.store(vcf.vcf_header.genotypes[j], Array.new)
          end

          alt_alleles = v.alt.split(",")
          a = ""

          r = v.ref + ":" + d["RO_ratio"].to_s
          ao_ratio_split = d["AO_ratio_split"]
          ao_ratio_split.each_with_index do |ao, i|
            alt = alt_alleles[i].eql?(".") ? "N" : alt_alleles[i]
            a += alt + ":" + ao.to_s
            if (i < ao_ratio_split.size - 1)
              a += ","
            end
          end

          massarray_data[vcf.vcf_header.genotypes[j]].push(r)
          massarray_data[vcf.vcf_header.genotypes[j]].push(a)
        end
      end
      f.puts massarray_header
      massarray_data.each do |k,v|
        f.puts k + "\t" + v.join("\t")
      end
    end
  end
end

def write_massarray(vcf,
                    loc_ids_file,
                    sample_ids_file,
                    allele_codes_file,
                    filename)

  loc_ids = Hash.new
  if (!loc_ids_file.eql?(""))
    File.open(loc_ids_file, "r") do |f|
      f.each_line do |line|
        sline = line.chomp.split("\t")
        loc_ids.store(sline[0], sline[1])
      end
    end
  end

  sample_ids = Hash.new
  if (!sample_ids_file.eql?(""))
    File.open(sample_ids_file, "r") do |f|
      f.each_line do |line|
        sline = line.chomp.split("\t")
        sample_ids.store(sline[0], sline[1])
      end
    end
  end

  a_codes = Hash.new
  if (!allele_codes_file.eql?(""))
    File.open(allele_codes_file, "r") do |f|
      f.each_line do |line|
        sline = line.chomp.split("\t")
        if(a_codes.has_key?(sline[0]))
          a_codes[sline[0]] = sline[1]
        else
          a_codes.store(sline[0], sline[1])
        end
      end
    end
  end

  massarray_header = ""
  massarray_data = Hash.new

  header = Array.new
  vcf.vcf_header.genotypes.each do |h|
    sample_head = h
    if (!sample_ids_file.eql?(""))
      if (sample_ids[h] != nil)
        sample_head = sample_ids[h]
      else
        raise RuntimeError, "ERROR: ID mapping for #{h} is missing!"
      end
    end
    massarray_data.store(sample_head, Array.new)
    header.push(sample_head)
  end

  vcf.vcf_entries.each do |k, vcf_entry|
    head = vcf_entry.chrom + "_" + vcf_entry.pos.to_s
    fhead = head
    if (!loc_ids_file.eql?(""))
      if (loc_ids[head] != nil)
        fhead = loc_ids[head]
      else
        raise RuntimeError, "ERROR: ID mapping for #{head} is missing!"
      end
    end
    massarray_header += "\t" + fhead + "\t" + fhead
    
    alt_arr = nil
    if (vcf_entry.alt.include?(","))
      alt_arr = vcf_entry.alt.split(',')
    else
      alt_arr = [vcf_entry.alt]
    end

    vcf_entry.genotypes.each_with_index do |gt, i|
      alleles = gt.split(":")[0].split("/")
      allele_codes = Array.new
      alleles.each do |a|
        if (!a.include?("."))
          if (a.to_i == 0)
            allele_codes.push(vcf_entry.ref)
          else
            allele_codes.push(alt_arr[a.to_i - 1])
          end
        else
          allele_codes = ["N", "N"]
        end
      end

      if (alleles.size == 1)
        allele_codes[1] = allele_codes[0]
      end

      if (!allele_codes_file.eql?(""))
        allele_codes[0] = a_codes[allele_codes[0]]
        allele_codes[1] = a_codes[allele_codes[1]]
      end

      massarray_data[header[i]].push(allele_codes[0])
      massarray_data[header[i]].push(allele_codes[1])
    end
  end

  open(filename, 'w') do |f|
    f.puts massarray_header

    massarray_data.each do |k, v|
      f.puts k + "\t" + v.join("\t")
    end
  end
end

def write_cmp(gt_cmp, filename)

  filename_data = "data_" + filename

  open(filename_data, 'w') do |f|
    f.puts gt_cmp.header.join("\t")
    gt_cmp.data.each do |k,v|
      f.puts v.join("\t")
    end
  end

  filename_gts1 = "gts1_" + filename
  filename_gts2 = "gts2_" + filename

  open(filename_gts1, 'w') do |f|
    gt_cmp.gt_dists.each do |k,v|
      f.puts k + "\t" + v[0].join("\t")
    end
  end

  open(filename_gts2, 'w') do |f|
    gt_cmp.gt_dists.each do |k,v|
      f.puts k + "\t" + v[1].join("\t")
    end
  end

  filename_cmp_gt = "cmp_gt_" + filename

  loc_name = Hash.new
  i = 0
  gt_cmp.cmp_dists.each do |k,v|
    loc_name.store(k,i)
    i += 1
  end

  tmp_gt_names = Array.new
  gt_cmp.cmp_dists.each do |k,v|
    v.each do |k2, v2|
      if(!tmp_gt_names.include?(k2))
        tmp_gt_names.push(k2)
      end
    end
  end

  tmp_gt_names.sort()

  gt_names = Hash.new
  j = 0

  tmp_gt_names.each do |n|
    gt_names.store(n,j)
    j += 1
  end

  cmp_dists_arr = Array.new(loc_name.size) { Array.new(tmp_gt_names.size,0) }

  gt_cmp.cmp_dists.each do |k,v|
    i = loc_name[k]
    v.each do |k2, v2|
      j = gt_names[k2]
      cmp_dists_arr[i][j] = v2
    end
  end

  open(filename_cmp_gt, 'w') do |f|
    #gt_cmp.cmp_dists.each do |k,v|
    #  f.puts k + "\t" + v.join("\t")
    #end
    f.puts "\t" + gt_names.keys().join("\t")
    for i in 0..cmp_dists_arr.size - 1
      str = loc_name.key(i)
      for j in 0..cmp_dists_arr[i].size - 1
        str += "\t" + cmp_dists_arr[i][j].to_s
      end
      f.puts str
    end
  end
end

end