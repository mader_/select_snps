#!/usr/bin/env ruby

=begin
  Copyright (c) 2020 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2020 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end

require 'optparse'
require 'ostruct'

class OptionParser
  def self.parse(args)

    options = OpenStruct.new
    options.vcf_file = ""
    options.loc_ids_file = ""
    options.sample_ids_file = ""
    options.allele_codes_file = ""
    options.output = "massarray_output.txt"

    optparse = OptionParser.new do |opts|
      opts.banner = "Usage: vcf2massarray.rb [options] -f vcf_file"

      opts.separator ""
      opts.separator "Options:"

      opts.on('-f', '--vcf-file [FILE]', "VCF file file for conversion") do |f|
        options.vcf_file = f
      end

      opts.on('-l', '--loc-mapping [FILE]', "Conversion of loc identifier") do |f|
        options.loc_ids_file = f
      end

      opts.on('-s', '--sample-mapping [FILE]', "Conversion of sample identifier") do |f|
        options.sample_ids_file = f
      end

      opts.on('-a', '--allele-codes [FILE]', "Text file with definitions of allele
                                     codes") do |f|
        options.allele_codes_file = f
      end

      opts.on('-o', '--output NAME', "Set the output filename (Default ",
               "massarray_output.txt)") do |f|
        options.output = f
      end

      opts.on('-h', '--help', 'Display this screen') do
        puts opts
        exit
      end

      opts.on_tail("--version", "Show version") do
        puts ""
        puts " Version: 0.1"
        puts ""
        puts "Copyright (c) 2020 Malte Mader <malte.mader@thuenen.de>"
        puts "Copyright (c) 2020 Thünen Institute of Forest Genetics"
        exit
      end
    end

    optparse.parse!(args)

    if(options.vcf_file.eql?(""))
      puts ""
      puts "A VCF file must be provided."
      puts ""
      puts optparse
      exit
    end

    return options
  end
end