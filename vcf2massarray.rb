#!/usr/bin/env ruby

=begin
  Copyright (c) 2020 Malte Mader <malte.mader@thuenen.de>
  Copyright (c) 2020 Thünen Institute of Forest Genetics
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
=end


require_relative 'lib/vcf'
require_relative 'lib/vcf_entry'
require_relative 'lib/plotter'
require_relative 'lib/vcf2massarray_option_parser.rb'

options = OptionParser.parse(ARGV)

vcf_file = options.vcf_file
loc_ids_file = options.loc_ids_file
sample_ids_file = options.sample_ids_file
allele_codes_file = options.allele_codes_file
out = options.output

begin

  vcf = VCF.new
  vcf.read_file(vcf_file, "single", 0, 1)

  plotter = Plotter.new

  plotter.write_massarray(vcf,
                          loc_ids_file,
                          sample_ids_file,
                          allele_codes_file,
                          out)

rescue RuntimeError => e 
  puts e.message
end